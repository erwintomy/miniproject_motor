<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>
<h3 align="center">Detail Pembelian Sparepart</h3><br>
<div class="box-body">
	<table class="table table-responsive table-striped table-bordered">
		<thead>
			<tr>
				<th>ID Beli Sparepart</th>
				<th>No Faktur</th>
				<th>Tanggal Beli</th>
				<th>Distributor ID</th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td>${item.id}</td>
				<td>${item.noFaktur}</td>
				<td>${item.tglBeli}</td>
				<td>${item.disSp.nama}</td>
			</tr>
		</tbody>
	</table>
</div>
<br>
<h4>Rincian Pembelian Sparepart :</h4>
<table class="table table-responsive table-bordered">
	<thead>
		<tr>
			<th>Nama Sparepart</th>
			<th>Harga</th>
			<th>Quantity</th>
			<th>Sub Total</th>
		</tr>
	</thead>
	<tbody id="list-detail">
		<c:forEach var="spDetail" items="${item.detail }" varStatus="count">
			<tr id="detail_${count.index }">
				<td><p id="detail_${count.index }_sparepartId" class="sparepartId" >${spDetail.bSp.nama}</p></td>
				<td><p id="detail_${count.index }_harga" class="harga" >${spDetail.harga }</p></td>
				<td><p id="detail_${count.index }_qty" class="qty" >${spDetail.qty }</p></td>
				<td><p id="detail_${count.index }_subtotal" class="subtotal" >${spDetail.subtotal }</p></td>
			</tr>
		</c:forEach>	
	</tbody>
</table>

<h2 align="center">Total  :  ${item.total}</h2>
<br>