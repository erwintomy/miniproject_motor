<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<fieldset>
	<legend>Form Pembelian Sparepart</legend>
</fieldset>
<form action="save" method="post" id="form-beliSparepart"
	class="form-horizontal">
	<div class="box-body">
		<input type="hidden" name="id" id="id" value="1" class="form-control">
		<input type="hidden" name="proses" id="proses" value="insert"
			class="form-control">

		     <div class="form-group">
				<label class="control-label col-md-2">No. Faktur</label>
				<div class="col-md-8">
					<input type="text" name="noFaktur" id="noFaktur" class="form-control"
					required="required" pattern="[A-Z0-9\s]{3,}"
					oninvalid="this.setCustomValidity('No Faktur harus diisi dan terdiri dari huruf capital dan angka')" 
                    onchange="this.setCustomValidity('')" />
                <div class="text-warning1"><p style="color:red">No Faktur Sudah Terpakai</p></div> 		
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-md-2">Tanggal Beli</label>
				<div class="col-md-8">
					<input name="tglBeli" id="tglBeli" class="form-control " 
					placeholder="yyyy-mm-dd" required="required"
					oninvalid="this.setCustomValidity('Tanggal tidak boleh kosong')" 
                	onchange="this.setCustomValidity('')"/>
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-md-2">Distributor</label>
				<div class="col-md-3">
					<select name="distributorId" id="distributorId" class="form-control" required="required"
				    oninvalid="this.setCustomValidity('Distributor harus dipilih')" 
                	onchange="this.setCustomValidity('')" >
							<option value="">---PILIH DISTRIBUTOR---</option>
							<c:forEach var="dis" items="${listDis }">
							<option value="${dis.id }"> ${dis.nama }</option>
						</c:forEach>
					</select>
				</div>
			</div>
			<fieldset>
					<legend>Detail Beli Sparepart</legend>
			</fieldset>
			<div class="form-group">
			   <label class="control-label col-md-2">Sparepart</label>
					<div class="col-md-3">
					   <select id="sparepartId" class="form-control">
					       <option value="">---PILIH SPAREPART---</option>
					       <c:forEach var="sp" items="${listSp }">
							<option value="${sp.id}" data-harga="${sp.harga}" data-stok="${sp.stok}"> ${sp.nama}</option>
						</c:forEach>
					   </select>
					</div>
					<div class="col-md-2">
					   <input type="text" id="harga" placeholder="Harga" class="form-control harga" readonly="readonly"/>
					</div>
					<div class="col-md-1">
					   <input type="text" id="qty" placeholder="Qty" class="form-control qty"/>
					</div>
					<div class="col-md-1">
						<input type="text" id="stok" placeholder="Stok" class="form-control" readonly="readonly" />
					</div>
					<div class="col-md-2">
					   <input type="text" id="subtotal" placeholder="Subtotal" class="form-control subtotal" readonly="readonly"/>
					</div>
					<div class="col-md-1">
					   <button type="button" id="btn-add-detail" class="btn btn-success required" ><i class="fa fa-plus"></i></button>
					</div>   
			  </div>
					<div class="text-warning2" style="padding-left:570px"><p style="color:red">Qty tidak boleh kosong dan harus angka</p></div>
			<div class="form-group">
			   <label class="control-label col-md-2"></label>
					<div class="col-md-8">
			   <table class="table table-responsive">
				   <thead>
				      <tr>
					      <th>Sparepart</th>
					      <th>Harga</th>
					      <th>Quantity</th>
					      <th>SubTotal</th>
					      <th>Action</th>
				      </tr>
				   </thead>
				   <tbody id="list-detail">

			      </tbody>   
			   </table>
			   </div>
			</div>
			
			<div class="form-group">
				<label class="control-label col-md-2">Total</label>
				<div class="col-md-8">
					<input type="text" name="total" id="total" value="0" class="form-control" readonly="readonly">
				</div>
			</div>
		</div>
	<div class="modal-footer">
		<button type="submit" class="btn btn-success">Simpan</button>
	</div>
</form>
<script>
$('#sparepartId').change(function(){
	var harga = $(this).find(':selected').attr('data-harga');
	var stok = $(this).find(':selected').attr('data-stok');
	$('#harga').val(harga);
	$('#stok').val(stok);
	var qty = 0;
	$('#qty').val(qty);
	var subTotal = parseInt(harga)*parseInt(qty);
	$('#subtotal').val(subTotal);
	$('#qty').focus();
	$('.btn').attr('disabled', false);
});

//qty diisi
$('#qty').keyup(function(){
	var harga = $('#harga').val();
	var qty = $(this).val();
	var subTotal = parseInt(harga)*parseInt(qty);
	$('#subtotal').val(subTotal);
});

$("#list-detail").on("keyup",".qty",function(e){
	var harga = $(this).parent().parent().find(".harga").val();
	var qty = $(this).val();
	var subTotal = parseInt(harga * qty);
	$(this).parent().parent().find(".subtotal").val(subTotal);
	
	var total = 0;
	$("#list-detail >tr").each(function(key,val){
		var itemTotal = $(this).find(".subtotal").val();
		total = total + parseInt(itemTotal); 
	});
	// menampilkan nilai total
	$("#total").val(total);
	
});

// tombol plus di klik
$('#btn-add-detail').click(function(){
	var qty = parseInt($('#qty').val());
	if (qty==''|| parseInt(qty)==0 || isNaN(qty)){
		$(".text-warning2").show();
	    $("#qty").focus();
	}
	else{
	var sparepartId =$('#sparepartId').val();
	var nama =$('#sparepartId :selected').text();
	var harga =$('#harga').val();
	var subtotal =$('#subtotal').val();
	var index = $('#list-detail >tr').length;
	
		var data ='<tr id="detail_'+ index +'">'+
		'<td>'+
			'<input type="hidden" name="detail['+ index +'].sparepartId" id="detail_'+ index +'_sparepartId" value="'+ sparepartId+'" class="form-control sparepartId" />'+ 
			nama +
		'</td>'+
		
		'<td><input type="text" name="detail['+ index +'].harga" id="detail_'+ index +'_harga" value="'+ harga+'" class="form-control harga" readonly="readonly" /></td>'+
		'<td><input type="text" name="detail['+ index +'].qty" id="detail_'+ index +'_qty" value="'+ qty+'" required="required" pattern="^[0-9]*" class="form-control qty"/></td>'+
		'<td><input type="text" name="detail['+ index +'].subtotal" id="detail_'+ index +'_subtotal" value="'+ subtotal +'" class="form-control subtotal" readonly="readonly" /></td>'+
		'<td><button type="button" class="btn btn-danger btn-delete-detail" value="'+ subtotal +'"><i class="fa fa-trash-o"></i></button></td>'+
	'</tr>';
			
	// menambahkan item ke list data	                
	$('#list-detail').append(data);
	
	// menambah total
	var total = parseInt($('#total').val());
	total = total + parseInt(subtotal);
	$('#total').val(total);
	
	// create data
	$('#sparepartId').val('');
	$('#harga').val('');	
	$('#qty').val('');
	$('#subtotal').val('');
	$('#stok').val('');
	$(".text-warning2").hide();
	$('#btn-add-detail').attr("disabled","disabled");
		}
});

// btn delete detail di klik
$("#list-detail").on("click",".btn-delete-detail",function(){
	var total = parseInt($('#total').val());
	var subtotal = parseInt($(this).val());
	
	total = total - subtotal;
	$('#total').val(total);
	
	$(this).parent().parent().remove();
	
	$.each($('#list-detail >tr'), function(index, item){
		var indexLama = $(this).attr('id').match(/\d+/);
		var trBaru = $(this).attr('id').replace('_'+indexLama,'_'+index);
		$(this).attr('id', trBaru);
		
        $.each($(this).find('.form-control'), function(key, val){
        	var namaLama = $(this).attr('name');
        	var namaBaru = namaLama.replace('['+indexLama+']','['+index+']');
        	$(this).attr('name', namaBaru);
        	
        	var idLama = $(this).attr('id');
        	var idBaru = idLama.replace('_'+indexLama+'_','_'+index+'_');
        	$(this).attr('id',idBaru);
        });
	});
});
	// setelah halaman selesai di load setelah pertama kali di load	
	$(document).ready(function() {
		$(".text-warning1").hide();
		$(".text-warning2").hide();
		$('#btn-add-detail').attr("disabled","disabled");
		
		$('#noFaktur').change(function(){
			var noFaktur = $('#noFaktur').val();
				$.ajax({
					url : 'beliSparepart/getByNo.json',
					type : 'post',
					data : {noFaktur:noFaktur},
					dataType : 'json',
					success : function(hasil) {
						if (hasil.data == 1) {
							$(".text-warning").show();
							$(':input[type="submit"]').prop('disabled', true);
						}
						else{
							$(".text-warning").hide();
							$(':input[type="submit"]').prop('disabled', false);
							}
						}	
			   });
		});
		
		$('#tglBeli').datepicker({
			autoclose:true,
		    format:'yyyy-mm-dd'
		});
	            
	});
</script>