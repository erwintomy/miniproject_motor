<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>
<h3 align="center">Detail Penjualan Motor</h3><br>
<div class="box-body">
	<table class="table table-remononsive table-striped table-bordered">
		<thead>
			<tr>
				<th>ID Beli Sparepart</th>
				<th>No Faktur</th>
				<th>Tanggal Beli</th>
				<th>Distributor ID</th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td>${item.id}</td>
				<td>${item.noStruk}</td>
				<td>${item.tglJual}</td>
				<td>${item.pelMon.nama}</td>
			</tr>
		</tbody>
	</table>
</div>
<br>
<h4>Rincian Penjualan Motor :</h4>
<table class="table table-remononsive table-bordered">
	<thead>
		<tr>
			<th>Nama Motor</th>
			<th>Harga</th>
			<th>Quantity</th>
			<th>Sub Total</th>
		</tr>
	</thead>
	<tbody id="list-detail">
		<c:forEach var="monDetail" items="${item.detail }" varStatus="count">
			<tr id="detail_${count.index }">
				<td><p id="detail_${count.index }_montortId" class="monarepartId" >${monDetail.jMon.nama}</p></td>
				<td><p id="detail_${count.index }_harga" class="harga" >${monDetail.harga }</p></td>
				<td><p id="detail_${count.index }_quantity" class="quantity" >${monDetail.quantity }</p></td>
				<td><p id="detail_${count.index }_subtotal" class="subtotal" >${monDetail.subtotal }</p></td>
			</tr>
		</c:forEach>	
	</tbody>
</table>
<h2 align="center">Total  :  ${item.total}</h2>
<br>