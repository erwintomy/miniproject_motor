<fieldset>
	<legend>Form Merk</legend>
</fieldset>
<form action="" method="post" id="form-merk" class="form-horizontal">
		<div class="box-body">
			<input type="hidden" name="id" id="id" value="${item.id }" class="form-control">
			<input type="hidden" name="merkTemp" id="merkTemp" value="${item.merk }" class="form-control">
			<input type="hidden" name="proses" id="proses" value="update" class="form-control">
			<div class="form-group">
				<label class="control-label col-md-2">Merk Motor</label>
				<div class="col-md-8">
					<input type="text" name="merk" id="merk" class="form-control" value="${item.merk }" pattern="[a-zA-Z\s]{3,}" required="required" maxlength="100"
			        oninvalid="this.setCustomValidity('Merk tidak boleh kosong dan harus huruf')" onchange="this.setCustomValidity('')" />
		            <div class="text-warning"><p style="color:red">Nama Merk Sudah Terpakai</p></div>
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-md-2">Asal Produsen</label>
				<div class="col-md-8">
					<input type="text" name="asal" id="asal" class="form-control" value="${item.asal }"
						required="required" maxlength="100" pattern="[a-zA-Z\s]{3,}"
						oninvalid="this.setCustomValidity('Asal tidak boleh kosong')" 
                    	onchange="this.setCustomValidity('')" />
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-md-2">Tahun Produksi</label>
				<div class="col-md-8">
					<input type="text" name="tahun" id="tahun" class="form-control" value="${item.tahun }"
						pattern="^[_0-9]{4,}$" required="required" maxlength="4"
						oninvalid="this.setCustomValidity('Tahun tidak boleh kosong & Harus angka (4 digits)')" 
                    	onchange="this.setCustomValidity('')" />
				</div>
			</div>
					
		</div>
		<div class="modal-footer">
		<button type="submit" class="btn btn-warning">Update</button>
	</div>
</form>
<script>
$(document).ready(function() {	
	$(".text-warning").hide();
	$('#merk').change(function(){
		var merkTemp = $('#merkTemp').val();
		var merk = $('#merk').val();
		if(merkTemp==merk) {
			$(':input[type="submit"]').prop('disabled', false);
			$(".text-warning").hide();
		}
		else {
			
		$.ajax({
			url : 'merk/getByNamaMerk.json',
			type : 'post',
			data : {merk:merk},
			dataType : 'json',
			success : function(hasil) {
				if (hasil.data == 1) {
					$(".text-warning").show();
					$(':input[type="submit"]').prop('disabled', true);
				}
				else{
					$(".text-warning").hide();
					$(':input[type="submit"]').prop('disabled', false);
				}
			}
	    });
		
		}
	});
});

</script>