<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>
<c:forEach var="item" items="${list}">
	<tr>
		<td>${item.id}</td>
		<td>${item.tpel}</td>
		<td>${item.nama}</td>
		<td>${item.alamat}</td>
		<td>${item.gender}</td>
		<td>${item.noKtp}</td>
		<td>${item.kota}</td>
		<td>${item.noTelp}</td>
		<td>
			<button type="button" class="btn btn-warning btn-edit"
				value="${item.id }">
				<i class="fa fa-edit"></i>
			</button>
			<button type="button" class="btn btn-danger btn-delete"
				value="${item.id }">
				<i class="fa fa-trash"></i>
			</button>
		</td>
	</tr>
</c:forEach>