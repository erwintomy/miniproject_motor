<fieldset>
	<legend>Form Pelanggan</legend>
</fieldset>
<form action="" method="post" id="form-pelanggan"
		class="form-horizontal">
		<div class="box-body">
			<input type="hidden" name="proses" id="proses" value="update" class="form-control">
			<input type="hidden" id="id" name="id" class="form-control" value="${item.id }">
			<input type="hidden" id="noKtpTemp" name="noKtpTemp" class="form-control" value="${item.noKtp }">
			<input type="hidden" id="tipe" name="tipe" class="form-control" value="${item.tpel }">
			<div class="form-group">
				<label class="control-label col-md-2">Tipe Pelanggan</label>
				<div class="col-md-8">
				<select name="Tpel" id="Tpel" class="form-control" required="required"
					oninvalid="this.setCustomValidity('Tipe harus dipilih')" 
                    onchange="this.setCustomValidity('')">
					<option value="">----PILIH PELANGGAN----</option>
					<option value="Motor">Motor</option>
					<option value="Sparepart">Sparepart</option>
				</select>
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-md-2">Nama</label>
				<div class="col-md-8">
					<input type="text" name="nama" id="nama" class="form-control" value="${item.nama }"
					pattern="[a-zA-Z\s]{3,}" required="required" maxlength="50"
					oninvalid="this.setCustomValidity('Nama tidak boleh kosong dan harus huruf')" 
                    onchange="this.setCustomValidity('')">
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-md-2">Alamat</label>
				<div class="col-md-8">
					<textarea name="alamat" id="alamat"
						class="form-control" required="required"
					oninvalid="this.setCustomValidity('Alamat tidak boleh kosong')" 
                    onchange="this.setCustomValidity('')">${item.alamat }</textarea>
				</div>
			</div>
			<div class="form-group">
			<label class="control-label col-md-2">Jenis Kelamin</label>
			<div class="col-md-6">
				<label class="radio-inline">
					<input type="radio" id="gender" name="gender" value="Laki-Laki" required="required"
					oninvalid="this.setCustomValidity('Jenis kelamin harus dipilih')" 
                    onchange="this.setCustomValidity('')">Laki-Laki
				</label>
				<label class="radio-inline">
					<input type="radio" id="gender" name="gender" value="Perempuan">Perempuan
				</label>
			</div>				
			</div>
			<div class="form-group">
				<label class="control-label col-md-2">No KTP</label>
				<div class="col-md-8">
					<input type="text" name="noKtp" id="noKtp" class="form-control" value="${item.noKtp }"
					required="required" pattern="^[_0-9]{16,}$" maxlength="16"
					oninvalid="this.setCustomValidity('No KTP harus diisi (16 digit)')" 
                    onchange="this.setCustomValidity('')">
                   <div class="text-warning"><p style="color:red">No KTP Sudah Terpakai</p></div> 
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-md-2">Kota</label>
				<div class="col-md-8">
					<input type="text" name="kota" id="kota" class="form-control" pattern="[a-zA-Z\s]{3,}" required="required"
					oninvalid="this.setCustomValidity('Kota tidak boleh kosong dan harus huruf')" 
                    onchange="this.setCustomValidity('')" value="${item.kota }">
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-md-2">No Telpon</label>
				<div class="col-md-8">
					<input type="text" name="noTelp" id="noTelp" class="form-control" value="${item.noTelp }"
					pattern="^[_0-9]{6,}$" required="required" maxlength="13"
					oninvalid="this.setCustomValidity('No Telepon tidak boleh kosong dan harus angka (6-13 digits)')" 
                    onchange="this.setCustomValidity('')">
				</div>
			</div>
		</div>
	<div class="modal-footer">
		<button type="submit" class="btn btn-warning">Update</button>
	</div>
</form>
<script>
$(document).ready(function() {
	var tipe = $("#tipe").val();
	$("#Tpel").val(tipe);
	$(".text-warning").hide();
	$('#noKtp').change(function(){
		var noKtp = $('#noKtp').val();
		var noKtpTemp = $('#noKtpTemp').val();
		if(noKtpTemp==noKtp) {
			$(':input[type="submit"]').prop('disabled', false);
			$(".text-warning").hide();
		}
		else {
		$.ajax({
			url : 'pelanggan/getByKTP.json',
			type : 'post',
			data : {noKtp:noKtp},
			dataType : 'json',
			success : function(hasil) {
				if (hasil.data == 1) {
					$(".text-warning").show();
					$(':input[type="submit"]').prop('disabled', true);
				}
				else{
					$(".text-warning").hide();
					$(':input[type="submit"]').prop('disabled', false);
				}
			}
      	    });
		}
	});
	
	var $radios = $('input:radio[name=gender]');
    if($radios.is(':checked') == false) {
        $radios.filter('[value=${item.gender}]').prop('checked', true);
    }
});
</script>	
