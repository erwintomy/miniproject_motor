<fieldset>
	<legend>Form Pelanggan</legend>
</fieldset>
<form action="" method="post" id="form-pelanggan"
		class="form-horizontal">

		<div class="box-body">
			<input type="hidden" name="id" id="id" value="1" class="form-control">
			<input type="hidden" name="proses" id="proses" value="insert" class="form-control">
			<div class="form-group">
				<label class="control-label col-md-2">Tipe Pelanggan</label>
				<div class="col-md-8">
				<select name="Tpel" id="Tpel" class="form-control" required="required"
					oninvalid="this.setCustomValidity('Tipe harus dipilih')" 
                    onchange="this.setCustomValidity('')">
					<option value="">----PILIH PELANGGAN----</option>
					<option value="Motor">Motor</option>
					<option value="Sparepart">Sparepart</option>
				</select>
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-md-2">Nama</label>
				<div class="col-md-8">
					<input type="text" name="nama" id="nama" class="form-control"
					pattern="[a-zA-Z\s]{3,}" required="required" maxlength="50"
					oninvalid="this.setCustomValidity('Nama tidak boleh kosong dan harus huruf')" 
                    onchange="this.setCustomValidity('')">
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-md-2">Alamat</label>
				<div class="col-md-8">
					<textarea name="alamat" id="alamat" class="form-control" required="required"
					oninvalid="this.setCustomValidity('Alamat tidak boleh kosong')" 
                    onchange="this.setCustomValidity('')"></textarea>
				</div>
			</div>
			<div class="form-group">
			<label class="control-label col-md-2">Jenis Kelamin</label>
			<div class="col-md-6">
				<label class="radio-inline">
					<input type="radio" id="gender" name="gender" value="Laki-Laki" checked="checked">Laki-Laki
				</label>
				<label class="radio-inline">
					<input type="radio" id="gender" name="gender" value="Perempuan" >Perempuan
				</label>
			</div>				
			</div>
			<div class="form-group">
				<label class="control-label col-md-2">No KTP</label>
				<div class="col-md-8">
					<input type="text" name="noKtp" id="noKtp" class="form-control"
					required="required" pattern="^[_0-9]{16,}$" maxlength="16"
					oninvalid="this.setCustomValidity('No KTP harus diisi (16 digit)')" 
                    onchange="this.setCustomValidity('')">
                    <div class="text-warning"><p style="color:red">No KTP Sudah Terpakai</p></div> 
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-md-2">Kota</label>
				<div class="col-md-8">
					<input type="text" name="kota" id="kota" class="form-control" pattern="[a-zA-Z\s]{3,}" required="required"
					oninvalid="this.setCustomValidity('Kota tidak boleh kosong dan harus huruf')" 
                    onchange="this.setCustomValidity('')">
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-md-2">No Telpon</label>
				<div class="col-md-8">
					<input type="text" name="noTelp" id="noTelp" class="form-control"
					pattern="^[_0-9]{6,}$" required="required" maxlength="13"
					oninvalid="this.setCustomValidity('No Telepon tidak boleh kosong dan harus angka (6 - 13)')" 
                    onchange="this.setCustomValidity('')">
				</div>
			</div>
		</div>
		<div class="modal-footer">
		<button type="submit" class="btn btn-success">Simpan</button>
	</div>
</form>
<script>
$(document).ready(function() {	
	$(".text-warning").hide();
	$('#noKtp').change(function(){
		var noKtp = $('#noKtp').val();
		$.ajax({
			url : 'pelanggan/getByKTP.json',
			type : 'post',
			data : {noKtp:noKtp},
			dataType : 'json',
			success : function(hasil) {
				if (hasil.data == 1) {
					$(".text-warning").show();
					$(':input[type="submit"]').prop('disabled', true);
				}
				else{
					$(".text-warning").hide();
					$(':input[type="submit"]').prop('disabled', false);
				}
			}
	});
	});
});

</script>