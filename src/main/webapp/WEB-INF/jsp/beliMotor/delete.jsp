<fieldset>
	<legend>Form Pembelian Motor</legend>
</fieldset>
<form id="form-beliMotor" method="post">
	<div class="form-horizontal">
		<input type="hidden" id="proses" name="proses" class="form-control" value="delete">
		<input type="hidden" id="id" name="id" class="form-control" value="${item.id }">
		
		<div align="center" class="form-group">
			<p>Apakah anda yakin mau menghapus data Pembelian " ${item.noFaktur} " ?</p>				
		</div>
	</div>

	<div class="modal-footer">
		<button type="submit" class="btn btn-danger">Delete</button>
	</div>
</form>
