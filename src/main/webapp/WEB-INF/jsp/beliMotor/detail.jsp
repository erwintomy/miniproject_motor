<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>
<h3 align="center">Detail Pembelian Motor</h3><br>
<div class="box-body">
	<table class="table table-responsive table-striped table-bordered">
		<thead>
			<tr>
				<th>ID Beli Motor</th>
				<th>No Faktur</th>
				<th>Tanggal Beli Motor</th>
				<th>Distributor ID</th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td>${item.id}</td>
				<td>${item.noFaktur}</td>
				<td>${item.tglBeli}</td>
				<td>${item.disMon.nama}</td>
			</tr>
		</tbody>
	</table>
</div>
<br>
<h4>Rincian Pembelian Sparepart :</h4>
<table class="table table-responsive table-bordered">
	<thead>
		<tr>
			<th>Nama Motor</th>
			<th>Harga</th>
			<th>Quantity</th>
			<th>Sub Total</th>
		</tr>
	</thead>
	<tbody id="list-detail">
		<c:forEach var="motorDetail" items="${item.detail }" varStatus="count">
			<tr id="detail_${count.index }">
				<td><p id="detail_${count.index }_motorId" class="motorId" >${motorDetail.bMon.nama}</p></td>
				<td><p id="detail_${count.index }_harga" class="harga" >${motorDetail.harga }</p></td>
				<td><p id="detail_${count.index }_quantity" class="quantity" >${motorDetail.quantity }</p></td>
				<td><p id="detail_${count.index }_subtotal" class="subtotal" >${motorDetail.subtotal }</p></td>
			</tr>
		</c:forEach>	
	</tbody>
</table>

<h2 align="center">Total  :  ${item.total}</h2>
<br>