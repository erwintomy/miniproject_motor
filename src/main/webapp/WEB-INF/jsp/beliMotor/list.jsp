<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>
<c:forEach var="item" items="${list}">
	<tr>
		<td>${item.id}</td>
		<td>${item.noFaktur}</td>
		<td>${item.tglBeli}</td>
		<td>${item.disMon.nama}</td>
		<td>${item.total}</td>
		<td>
			<button type="button" class="btn btn-warning btn-edit"
				value="${item.id }">
				<i class="fa fa-edit"></i>
			</button>
			<button type="button" class="btn btn-danger btn-delete"
				value="${item.id }">
				<i class="fa fa-trash"></i>
			</button>
			<button type="button" class="btn btn-success btn-detail"
				value="${item.id }">
				<i class="fa fa-eye"></i>
			</button>
		</td>
	</tr>
</c:forEach>