<% request.setAttribute("contextName", request.getContextPath()); %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://www.opensymphony.com/sitemesh/decorator" prefix="decorator"%>
<%@ taglib uri="http://www.opensymphony.com/sitemesh/page" prefix="page"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<sec:authorize access="isAuthenticated()">
    <sec:authentication property="principal.username" var="username" />
</sec:authorize>
<sec:authorize access="!isAuthenticated()">
    <sec:authentication property="principal" var="username" />
</sec:authorize>

<h2 align="center">Welcome,  ${username}</h2><br>
<!-- <div class="col-md-3 col-sm-6 col-xs-12">
	<div class="info-box">
		<span class="info-box-icon bg-aqua"><i
			class="ion ion-ios-gear-outline"></i></span>

		<div class="info-box-content">
			<span class="info-box-text">Keuntungan</span> <span
				class="info-box-number">90<small>%</small></span>
		</div>
		/.info-box-content
	</div>
	/.info-box
</div>

<div class="col-md-3 col-sm-6 col-xs-12">
	<div class="info-box">
		<span class="info-box-icon bg-green"><i
			class="ion ion-ios-cart-outline"></i></span>

		<div class="info-box-content">
			<span class="info-box-text">Penjualan</span> <span
				class="info-box-number">760</span>
		</div>
		/.info-box-content
	</div>
	/.info-box
</div>

<div class="col-md-3 col-sm-6 col-xs-12">
	<div class="info-box">
		<span class="info-box-icon bg-yellow"><i
			class="ion ion-ios-people-outline"></i></span>

		<div class="info-box-content">
			<span class="info-box-text">Jumlah Pelanggan</span> <span
				class="info-box-number">2,000</span>
		</div>
		/.info-box-content
	</div>
	/.info-box
</div>

<div class="col-md-4">
	Info Boxes Style 2
	<div class="info-box bg-yellow">
		<span class="info-box-icon"><i
			class="ion ion-ios-pricetag-outline"></i></span>

		<div class="info-box-content">
			<span class="info-box-text">Inventory</span> <span
				class="info-box-number">5,200</span>

			<div class="progress">
				<div class="progress-bar" style="width: 50%"></div>
			</div>
			<span class="progress-description"> 50% Increase in 30 Days </span>
		</div>
		/.info-box-content
	</div>
	/.info-box
	<div class="info-box bg-green">
		<span class="info-box-icon"><i
			class="ion ion-ios-heart-outline"></i></span>

		<div class="info-box-content">
			<span class="info-box-text">Mentions</span> <span
				class="info-box-number">92,050</span>

			<div class="progress">
				<div class="progress-bar" style="width: 20%"></div>
			</div>
			<span class="progress-description"> 20% Increase in 30 Days </span>
		</div>
		/.info-box-content
	</div>
	/.info-box
	<div class="info-box bg-red">
		<span class="info-box-icon"><i
			class="ion ion-ios-cloud-download-outline"></i></span>

		<div class="info-box-content">
			<span class="info-box-text">Downloads</span> <span
				class="info-box-number">114,381</span>

			<div class="progress">
				<div class="progress-bar" style="width: 70%"></div>
			</div>
			<span class="progress-description"> 70% Increase in 30 Days </span>
		</div>
		/.info-box-content
	</div> -->