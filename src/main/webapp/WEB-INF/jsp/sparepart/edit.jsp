<fieldset>
	<legend>Form Sparepart</legend>
</fieldset>
<form action="" method="post" id="form-sparepart"
	class="form-horizontal">
	<div class="box-body">
		<input type="hidden" name="id" id="id" value="${item.id }" class="form-control">
		<input type="hidden" name="proses" id="proses" value="update">
		<div class="form-group">
			<label class="control-label col-md-2">Nama Sparepart</label>
			<div class="col-md-8">
				<input type="text" name="nama" id="nama" class="form-control" value="${item.nama }"
					required="required" maxlength="100"
					oninvalid="this.setCustomValidity('Nama Sparepart tidak boleh kosong')" 
                    onchange="this.setCustomValidity('')" />
			</div>
		</div>
		<div class="form-group">
			<label class="control-label col-md-2">Merk</label>
			<div class="col-md-8">
				<input type="text" name="merk" id="merk" class="form-control" value="${item.merk }"
					required="required" maxlength="100" pattern="[a-zA-Z\s]{3,}"
					oninvalid="this.setCustomValidity('Merk tidak boleh kosong dan harus huruf')" 
                    onchange="this.setCustomValidity('')"  />
			</div>
		</div>
		<div class="form-group">
			<label class="control-label col-md-2">Stok</label>
			<div class="col-md-8">
				<input type="text" name="stok" id="stok" class="form-control" value="${item.stok }"
					required="required" maxlength="100" pattern="^[_0-9]{1,}$"
					oninvalid="this.setCustomValidity('Stok tidak boleh kosong dan harus angka')" 
                    onchange="this.setCustomValidity('')"  />
			</div>
		</div>
		<div class="form-group">
			<label class="control-label col-md-2">Harga</label>
			<div class="col-md-8">
				<input type="text" name="harga" id="harga" class="form-control" value="${item.harga }"
					pattern="^[_0-9]{1,}$" required="required"
					oninvalid="this.setCustomValidity('Harga tidak boleh kosong dan harus angka')" 
                    onchange="this.setCustomValidity('')" />
			</div>
		</div>
	</div>
	<div class="modal-footer">
		<button type="submit" class="btn btn-warning">Update</button>
	</div>
</form>