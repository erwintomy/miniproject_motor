<div class="box box-success">
	<div class="box-header">
		<h3 class="box-title">Data Admin</h3>
		<br><br>
		<table>
	    <tr><td width="420px">
	    <div class="form-inline">
	    <div class="form-inline pull-right">
			<label class="control-label" style="padding-right: 20px">Pencarian</label>
				<input type="text" id="search" placeholder="Type to search" class="form-control"></div>
				<button type="button" id="btn-add" class="btn btn-success pull-left"><i class="fa fa-plus"></i> Tambah Data</button>
		</div>
		</td>
	    </tr>
	</table>			
	</div>
	<div class="box-body">
		<table class="table table-responsive table-striped table-bordered">
			<thead>
				<tr>
					<th>Username</th>
					<th>Password</th>
					<th>Action</th>
				</tr>
			</thead>
			<tbody id="list-data">

			</tbody>
		</table>
	</div>
</div>

<!-- Modal -->
<div id="modal-input" class="modal" >
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header" style="background:#00a65a">
				<button type="button" class="close" data-dismiss="modal">
					<i class="fa fa-close" style="color:white"></i>
				</button>
			</div>
			<div class="modal-body">
			
			</div>
		</div>
	</div>
</div>


<script>
function loadData() {
	$.ajax({
		url:'admin/list.html',
		type:'get',
		dataType:'html',
		success:function(data){
			$("#list-data").html(data);
		}
	});
}

$('#search').keyup(function(){
	var $rows = $('#list-data tr');
	var val = $.trim($(this).val()).replace(/ +/g, ' ').toLowerCase();
    $rows.show().filter(function() {
        var text = $(this).text().replace(/\s+/g, ' ').toLowerCase();
        return !~text.indexOf(val);
    }).hide();
});

$("#btn-add").on("click",function(){
	$.ajax({
		url:'admin/add.html',
		type:'get',
		dataType:'html',
		success:function(data){
			$("#modal-input").find(".modal-body").html(data);
			$("#modal-input").modal('show');
		}
	});
	
});

$("#list-data").on("click",".btn-delete",function(){
	var vId = $(this).val();
	$.ajax({
		url:'admin/delete.html',
		type:'get',
		data:{ id:vId },
		dataType:'html',
		success:function(data){
			$("#modal-input").find(".modal-body").html(data);
			$("#modal-input").modal('show');
		}
	});
});

$("#modal-input").on("submit","#form-register",function(){
	$.ajax({
		url:'admin/save.json',
		type:'post',
		data:$(this).serialize(),
		dataType:'json',
		success:function(data){
			if(data.result=="berhasil"){
				$("#modal-input").modal('hide');
				loadData();
			}
			else{
				$("#modal-input").modal('show');
				alert(data.message);
			}
		}
	});
	return false;
});

$("#list-data").on("click",".btn-edit",function(){
	var vId = $(this).val();
	$.ajax({
		url:'admin/edit.html',
		type:'get',
		data:{ id:vId },
		dataType:'html',
		success:function(data){
			$("#modal-input").find(".modal-body").html(data);
			$("#modal-input").modal('show');
		}
	});
});

	// setelah halaman selesai di load setelah pertama kali di load	
	$(document).ready(function() {
		// panggil method loadData
		loadData();
	});
</script>
