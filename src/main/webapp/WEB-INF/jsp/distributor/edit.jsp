<fieldset>
	<legend>Form Distributor</legend>
</fieldset>
<form action="" method="post" id="form-distributor"
		class="form-horizontal">

		<div class="box-body">
			<input type="hidden" id="id" name="id" class="form-control" value="${item.id }">
			<input type="hidden" id="namaTemp" name="namaTemp" class="form-control" value="${item.nama }">
			<input type="hidden" id="tipe" name="tipe" class="form-control" value="${item.tdis }">
			<input type="hidden" name="proses" id="proses" value="update" class="form-control">
			<div class="form-group">
				<label class="control-label col-md-2">Tipe Distributor</label>
				<div class="col-md-8">
				<select name="Tdis" id="Tdis" class="form-control"
						required="required"
						oninvalid="this.setCustomValidity('Tipe harus dipilih!')" 
                    	onchange="this.setCustomValidity('')">
					<option value="">----PILIH TIPE DISTRIBUTOR----</option>
					<option value="Motor">Motor</option>
					<option value="Sparepart">Sparepart</option>
				</select>
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-md-2">Nama</label>
				<div class="col-md-8">
					<input type="text" name="nama" id="nama" class="form-control" value="${item.nama }"
					pattern="[a-zA-Z\s]{3,}" required="required" maxlength="50"
					oninvalid="this.setCustomValidity('Nama tidak boleh kosong dan harus huruf!')" 
                    onchange="this.setCustomValidity('')" >
                    <div class="text-warning"><p style="color:red">Nama Sudah Terpakai</p></div> 
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-md-2">Alamat</label>
				<div class="col-md-8">
					<textarea name="alamat" id="alamat" required="required" 
						oninvalid="this.setCustomValidity('Alamat tidak boleh kososng!')" 
                    	onchange="this.setCustomValidity('')"
						class="form-control">${item.alamat}</textarea>
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-md-2">Kota</label>
				<div class="col-md-8">
					<input type="text" name="kota" id="kota" class="form-control" value="${item.kota }"
						required="required" pattern="[a-zA-Z\s]{3,}"
						oninvalid="this.setCustomValidity('Alamat tidak boleh kosong dan harus huruf!')" 
                    	onchange="this.setCustomValidity('')">
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-md-2">No Telpon</label>
				<div class="col-md-8">
					<input type="text" name="noTelp" id="noTelp" class="form-control" value="${item.noTelp }"
					pattern="^[_0-9]{6,}$" required="required" maxlength="13"
					oninvalid="this.setCustomValidity('No Telepon tidak boleh kosong dan harus angka!')" 
                    onchange="this.setCustomValidity('')" >
				</div>
			</div>
		</div>
		<div class="modal-footer">
		<button type="submit" class="btn btn-warning">Update</button>
	</div>
</form>
<script>
$(document).ready(function() {
	var tipe = $("#tipe").val();
	$("#Tdis").val(tipe);
	$(".text-warning").hide();
	$('#nama').change(function(){
		var namaTemp = $('#namaTemp').val();
		var nama = $('#nama').val();
		if(namaTemp==nama) {
			$(':input[type="submit"]').prop('disabled', false);
			$(".text-warning").hide();
		}
		else {
		$.ajax({
			url : 'distributor/getByName.json',
			type : 'post',
			data : {nama:nama},
			dataType : 'json',
			success : function(hasil) {
				if (hasil.data == 1) {
					$(".text-warning").show();
					 $(':input[type="submit"]').prop('disabled', true);
				}
				else{
					$(".text-warning").hide();
					 $(':input[type="submit"]').prop('disabled', false);
					}
				}
		  });
		}
	});
});
</script>