<fieldset>
	<legend>Form Distributor</legend>
</fieldset>
<form id="form-distributor" action="update" method="post">
	<div class="form-horizontal">
		<input type="hidden" id="proses" name="proses" class="form-control" value="delete">
		<input type="hidden" id="id" name="id" class="form-control" value="${item.id }">
		
		<div align="center" class="form-group">
			<p>Apakah anda yakin mau menghapus data Distributor " ${item.nama} " ?</p>				
		</div>
	</div>

	<div class="modal-footer">
		<button type="submit" class="btn btn-danger">Delete</button>
	</div>
</form>
