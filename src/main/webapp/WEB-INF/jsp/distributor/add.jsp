<fieldset>
	<legend>Form Distributor</legend>
</fieldset>
<form action="" method="post" id="form-distributor"
		class="form-horizontal">
		<div class="box-body">
			<input type="hidden" name="id" id="id" value="1" class="form-control">
			<input type="hidden" name="proses" id="proses" value="insert" class="form-control">
			<div class="form-group">
				<label class="control-label col-md-2">Tipe Distributor</label>
				<div class="col-md-8">
				<select name="Tdis" id="Tdis" class="form-control" required="required"
						oninvalid="this.setCustomValidity('Tipe harus dipilih!')" 
                    	onchange="this.setCustomValidity('')">
					<option value="">----PILIH TIPE DISTRIBUTOR----</option>
					<option value="Motor">Motor</option>
					<option value="Sparepart">Sparepart</option>
				</select>
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-md-2">Nama</label>
				<div class="col-md-8">
					<input type="text" name="nama" id="nama" class="form-control"
					pattern="[a-zA-Z\s]{3,}" required="required" maxlength="50"
					oninvalid="this.setCustomValidity('Nama tidak boleh kosong dan harus huruf!')" 
                    onchange="this.setCustomValidity('')" >
                    <div class="text-warning"><p style="color:red">Nama Sudah Terpakai</p></div> 
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-md-2">Alamat</label>
				<div class="col-md-8">
					<textarea name="alamat" id="alamat" required="required"
						oninvalid="this.setCustomValidity('Alamat tidak boleh kososng!')" 
                    	onchange="this.setCustomValidity('')"
						class="form-control"></textarea>
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-md-2">Kota</label>
				<div class="col-md-8">
					<input type="text" name="kota" id="kota" required="required" pattern="[a-zA-Z\s]{3,}"
						oninvalid="this.setCustomValidity('Kota tidak boleh kososng dan harus huruf')" 
                    	onchange="this.setCustomValidity('')"class="form-control">
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-md-2">No Telpon</label>
				<div class="col-md-8">
					<input type="text" name="noTelp" id="noTelp" class="form-control" 
					pattern="^[0-9]{6,}" required="required" maxlength="13"
					oninvalid="this.setCustomValidity('No Telepon tidak boleh kosong dan harus angka!')" 
                    onchange="this.setCustomValidity('')" >
				</div>
			</div>
		</div>
		<div class="modal-footer">
		<button type="submit" class="btn btn-success">Simpan</button>
	</div>
</form>
<script>
$(document).ready(function() {
	$(".text-warning").hide();
	$('#nama').change(function(){
		var nama = $('#nama').val();
		$.ajax({
			url : 'distributor/getByName.json',
			type : 'post',
			data : {nama:nama},
			dataType : 'json',
			success : function(hasil) {
				if (hasil.data == 1) {
					$(".text-warning").show();
					 $(':input[type="submit"]').prop('disabled', true);
				}
				else{
					$(".text-warning").hide();
					 $(':input[type="submit"]').prop('disabled', false);
					}
				}
		  });
	});
});

</script>