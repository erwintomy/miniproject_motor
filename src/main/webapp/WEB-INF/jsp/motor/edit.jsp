<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<fieldset>
	<legend>Form Motor</legend>
</fieldset>
<form action="" method="post" id="form-motor"
	class="form-horizontal">
	<div class="box-body">
		<input type="hidden" name="id" id="id" value="${item.id }" class="form-control">
		<input type="hidden" name="noRangkaTemp" id="noRangkaTemp" value="${item.noRangka }" class="form-control">
		<input type="hidden" name="proses" id="proses" value="update">
		<div class="form-group">
				<label class="control-label col-md-2">Nama Motor</label>
				<div class="col-md-8">
					<input type="text" name="nama" id="nama" class="form-control" value="${item.nama }"					
					required="required" maxlength="20"
					oninvalid="this.setCustomValidity('Nama tidak boleh kosong')" 
                    onchange="this.setCustomValidity('')">
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-md-2">Merk</label>
				<div class="col-md-4">
					<Select name="merkId" id="merkId" class="form-control" required="required"
					oninvalid="this.setCustomValidity('Merk Motor harus di pilih')" 
                    onchange="this.setCustomValidity('')">
						<option value="">Pilih Merk</option>
						<c:forEach var="merk" items="${listMerk }">
							<option value="${merk.id }" ${merk.id == item.merkId ?'selected="selected"':'' }> ${merk.merk }</option>
				    </c:forEach>
					</Select>
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-md-2">No Rangka</label>
				<div class="col-md-8">
					<input type="text" name="noRangka" id="noRangka" class="form-control" value="${item.noRangka }"
					required="required" pattern="[A-Z0-9\s]{3,}"
					oninvalid="this.setCustomValidity('No Rangka harus diisi dan terdiri dari huruf capital dan angka')" 
                    onchange="this.setCustomValidity('')">
                    <div class="text-warning"><p style="color:red">No Rangka Sudah Terpakai</p></div> 
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-md-2">Deskripsi</label>
				<div class="col-md-8">
					<textarea name="deskripsi" id="deskripsi" class="form-control" 
					required="required"
					oninvalid="this.setCustomValidity('Deskripsi harus diisi')" 
                    onchange="this.setCustomValidity('')">${item.deskripsi }</textarea>
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-md-2">Stok</label>
				<div class="col-md-8">
					<input type="text" name="stok" id="stok" class="form-control" value="${item.stok }"
					required="required" pattern="^[_0-9]{1,}$"
					oninvalid="this.setCustomValidity('Stok harus diisi dan harus angka')" 
                    onchange="this.setCustomValidity('')">
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-md-2">Harga</label>
				<div class="col-md-8">
					<input type="text" name="harga" id="harga" class="form-control" value="${item.harga }"
					required="required" pattern="^[_0-9]{3,}$" maxlength="10"
					oninvalid="this.setCustomValidity('Harga harus diisi dan harga harus angka')" 
                    onchange="this.setCustomValidity('')">
				</div>
			</div>
		</div>
	<div class="modal-footer">
		<button type="submit" class="btn btn-warning">Update</button>
	</div>
</form>
<script>
$(document).ready(function() {
	$(".text-warning").hide();
	$('#noRangka').change(function(){
		var noRangka = $('#noRangka').val();
		var noRangkaTemp = $('#noRangkaTemp').val();
		if(noRangkaTemp==noRangka) {
			$(':input[type="submit"]').prop('disabled', false);
			$(".text-warning").hide();
		}
		else {
		
		$.ajax({
			url : 'motor/getByNoRangka.json',
			type : 'post',
			data : {noRangka:noRangka},
			dataType : 'json',
			success : function(hasil) {
				if (hasil.data == 1) {
					$(".text-warning").show();
					 $(':input[type="submit"]').prop('disabled', true);
				}
				else{
					$(".text-warning").hide();
					 $(':input[type="submit"]').prop('disabled', false);
					}
				}
		  });
		}
	});
});
</script>