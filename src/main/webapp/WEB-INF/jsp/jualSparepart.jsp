<div class="box box-success">
	<div class="box-header">
		<h3 class="box-title">Data Penjualan Sparepart</h3>
		<br><br>
		<table>
	    <tr><td width="420px">
	    <div class="form-inline">
	    <div class="form-inline pull-right">
			<label class="control-label" style="padding-right: 20px">Pencarian</label>
				<input type="text" id="search" placeholder="Type to search" class="form-control"></div>
				<button type="button" id="btn-add" class="btn btn-success pull-left"><i class="fa fa-plus"></i> Tambah Data</button>
		</div>
		</td>
	    </tr>
	</table>	
	</div>
	<div class="box-body">
		<table class="table table-responsive table-striped table-bordered">
			<thead>
				<tr>
					<th>ID Jual Sparepart</th>
					<th>No Struk</th>
					<th>Tanggal Jual Sparepart</th>
					<th>Pelanggan ID</th>
					<th>Total</th>
					<th>Action</th>
				</tr>
			</thead>
			<tbody id="list-data">

			</tbody>
		</table>
	</div>
</div>

<!-- Modal -->
<div id="modal-input" class="modal" >
	<div class="modal-dialog" style="width: 1000px">
		<div class="modal-content">
			<div class="modal-header" style="background:#00a65a">
				<button type="button" class="close" data-dismiss="modal">
					<i class="fa fa-close" style="color:white"></i>
				</button>
			</div>
			<div class="modal-body">
			
			</div>
		</div>
	</div>
</div>

<script>
	function loadData() {
		$.ajax({
			url:'jualSparepart/list.html',
			type:'get',
			dataType:'html',
			success:function(data){
				$("#list-data").html(data);
			}
		});
	}
	
	$('#search').keyup(function(){
		var $rows = $('#list-data tr');
		var val = $.trim($(this).val()).replace(/ +/g, ' ').toLowerCase();
	    $rows.show().filter(function() {
	        var text = $(this).text().replace(/\s+/g, ' ').toLowerCase();
	        return !~text.indexOf(val);
	    }).hide();
	});
	
	$("#btn-add").on("click",function(){
		$.ajax({
			url:'jualSparepart/add.html',
			type:'get',
			dataType:'html',
			success:function(data){
				$("#modal-input").find(".modal-body").html(data);
				$("#modal-input").modal('show');
			}
		});	
	});
	
	$("#list-data").on("click",".btn-delete",function(){
		var vId = $(this).val();
		$.ajax({
			url:'jualSparepart/delete.html',
			type:'get',
			data:{ id:vId },
			dataType:'html',
			success:function(data){
				$("#modal-input").find(".modal-body").html(data);
				$("#modal-input").modal('show');
			}
		});
	});
	
	$("#modal-input").on("submit","#form-jualSparepart",function(){
		$.ajax({
			url:'jualSparepart/save.json',
			type:'post',
			data:$(this).serialize(),
			dataType:'json',
			success:function(data){
				if(data.result=="berhasil"){
					$("#modal-input").modal('hide');
					loadData();
				}
				else{
					$("#modal-input").modal('show');
				}
			}
		});
		return false;
	});
    
	$("#list-data").on("click",".btn-edit",function(){
		var vId = $(this).val();
		$.ajax({
			url:'jualSparepart/edit.html',
			type:'get',
			data:{ id:vId },
			dataType:'html',
			success:function(data){
				$("#modal-input").find(".modal-body").html(data);
				$("#modal-input").modal('show');
			}
		});
	});
	
	$("#list-data").on("click", ".btn-edit", function() {
		var id = $(this).val();
		$.ajax({
			url : 'jualSparepart/getById.json',
			type : 'post',
			data : {id:id},
			dataType : 'json',
			success : function(hasil) {
				if (hasil.result == "success") {
					$('#tglJual').datepicker('update',hasil.data.tglJual);
					// di kosongkan dulu
				 	$('#list-detail').empty();
					// load detail jualSparepart
					$.each(hasil.data.detail, function(index, item){
							var data ='<tr id="detail_'+ index +'">'+
							'<td>'+
								'<input type="hidden" name="detail['+ index +'].sparepartId" id="detail_'+ index +'_sparepartId" value="'+ item.sparepartId+'" class="form-control" />'+ 
								item.sparepartId +
							'</td>'+
							'<td><input type="text" name="detail['+ index +'].harga" id="detail_'+ index +'_harga" value="'+ item.harga+'" class="form-control" /></td>'+
							'<td><input type="text" name="detail['+ index +'].quantity" id="detail_'+ index +'_quantity" value="'+ item.quantity+'" class="form-control" /></td>'+
							'<td><input type="text" name="detail['+ index +'].subtotal" id="detail_'+ index +'_subtotal" value="'+ item.subtotal +'" class="form-control" /></td>'+
							'<td><button type="button" class="btn btn-danger btn-delete-detail" value="'+ item.subtotal +'"><i class="fa fa-trash-o"></i></button></td>'+
						    '</tr>';
						
						$('#list-detail').append(data);
					});
				}
			}
		});
	});
	
	$("#list-data").on("click",".btn-detail",function(){
		var vId = $(this).val();
		$.ajax({
			url:'jualSparepart/detail.html',
			type:'get',
			data:{ id:vId },
			dataType:'html',
			success:function(data){
				$("#modal-input").find(".modal-body").html(data);
				$("#modal-input").modal('show');
			}
		});
	});
	
	$(document).ready(function() {
		loadData();
	});
</script>
