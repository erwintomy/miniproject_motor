<fieldset>
	<legend>Form Register</legend>
</fieldset>
<form action="" method="post" id="form-register"
	class="form-horizontal">
	<div class="box-body">
		<input type="hidden" name="id" id="id" value="${item.id}" class="form-control" />
		<input type="hidden" name="roleId" id="roleId" value="1" class="form-control" />
		<input type="hidden" name="active" id="active" value="1" class="form-control" />
		<input type="hidden" name="proses" id="proses" value="update" />
		<div class="form-group">
			<label class="control-label col-md-2">Username</label>
			<div class="col-md-8">
				<input type="text" name="username" id="username" value="${item.username}" class="form-control" />
			</div>
		</div>
		<div class="form-group">
			<label class="control-label col-md-2">Password</label>
			<div class="col-md-8">
				<input type="text" name="password" id="password" value="${item.password}" class="form-control"  />
			</div>
		</div>		
	</div>
	<div class="modal-footer">
		<button type="submit" class="btn btn-warning">Update</button>
	</div>
</form>
