<aside class="main-sidebar">
	<!-- sidebar: style can be found in sidebar.less -->
	<section class="sidebar">
		<!-- Sidebar user panel -->
		<div class="user-panel">
			<div class="pull-left image">
				<img src="${contextName}/assets/dist/img/avatar5.png"
					class="img-circle" alt="User Image">
			</div>
			<div class="pull-left info">
				<b>${username}</b><br><br>
				<a href="#"><i class="fa fa-circle text-success"></i> Online</a>
			</div>
		</div>
		<!-- sidebar menu: : style can be found in sidebar.less -->
		<ul class="sidebar-menu">
			<li class="header">MAIN MENU</li>
			<li class="treeview">
				<a href="#"> 
					<i	class="fa fa-id-card-o"></i> <span>Master Data</span> 
					<span class="pull-right-container"> <i class="fa fa-angle-left pull-right"></i>	</span>
				</a>
				<ul class="treeview-menu">
					<li><a href="${contextName}/admin.html"><i class="fa fa-user"></i> User Admin</a></li>
					<li><a href="${contextName}/distributor.html" class="menu-item"><i class="fa fa-truck"></i> Distributor</a></li>
					<li><a href="${contextName}/pelanggan.html"><i class="fa fa-users"></i> Pelanggan</a></li>
					<li><a href="${contextName}/motor.html"><i class="fa fa-motorcycle"></i> Motor</a></li>
					<li><a href="${contextName}/merk.html"><i class="fa fa-apple"></i> Merk</a></li>
					<li><a href="${contextName}/sparepart.html"><i class="fa fa-steam"></i> Sparepart</a></li>
				</ul>
			</li>
			<li>
				<a href="#"> 
					<i	class="fa fa-line-chart"></i> <span>Transaksi Penjualan</span> 
					<span class="pull-right-container"> <i class="fa fa-angle-left pull-right"></i>	</span>
				</a>
				<ul class="treeview-menu">
					<li><a href="${contextName}/jualMotor.html" class="menu-item"><i class="fa fa-motorcycle"></i>  Penjualan Motor</a></li>
					<li><a href="${contextName}/jualSparepart.html" class="menu-item"><i class="fa fa-steam"></i>  Penjualan Sparepart</a></li>	    
				</ul>
			</li>
			<li>
				<a href="#"> 
					<i	class="fa fa-shopping-cart"></i> <span>Transaksi Pembelian</span> 
					<span class="pull-right-container"> <i class="fa fa-angle-left pull-right"></i>	</span>
				</a>
				<ul class="treeview-menu">
					<li><a href="${contextName}/beliMotor.html" class="menu-item"><i class="fa fa-motorcycle"></i>  Pembelian Motor</a></li>	    
					<li><a href="${contextName}/beliSparepart.html" class="menu-item"><i class="fa fa-steam"></i>  Pembelian Sparepart</a></li>
				</ul>
			</li>
		</ul>
	</section>
	<!-- /.sidebar -->
</aside>