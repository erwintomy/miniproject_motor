<footer class="main-footer">
   <div class="pull-right hidden-xs">
     <b>Version</b> 2.3.8
   </div>
   <strong>Copyright &copy; 2016-2017 <a>PT Tuturu Motor & Sparepart </a>.</strong> All rights
   reserved.
 </footer>