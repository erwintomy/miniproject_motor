package com.xsis.miniproject.model;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.TableGenerator;


@Entity
@Table(name="SPAREPART")

public class SparepartModel {
	
	@Id
	@Column(name="ID")
	@GeneratedValue(strategy=GenerationType.TABLE, generator="SPAREPART")
	@TableGenerator(name="SPAREPART",table="SEQUENCE", pkColumnName="ID",
	pkColumnValue="SPAREPART", valueColumnName="VALUE", allocationSize=1, initialValue=1)
	private int id;
	
	@Column(name="NAMA")
	private String nama;
	
	@Column(name="MERK")
	private String merk;
	
	@Column(name="Stok")
	private Integer Stok;
	
	public Integer getStok() {
		return Stok;
	}

	public void setStok(Integer stok) {
		Stok = stok;
	}

	@Column(name="HARGA")
	private int harga;
    
	@OneToMany(mappedBy="jSp"/*, fetch=FetchType.EAGER*/)
	private List<JualSparepartDetailModel> jSp;
	
	@OneToMany(mappedBy="bSp"/*, fetch=FetchType.LAZY*/)
/*	@LazyCollection(LazyCollectionOption.FALSE)*/
	private List<BeliSparepartDetailModel> bSp;

	public List<JualSparepartDetailModel> getjSp() {
		return jSp;
	}

	public void setjSp(List<JualSparepartDetailModel> jSp) {
		this.jSp = jSp;
	}

	public List<BeliSparepartDetailModel> getbSp() {
		return bSp;
	}

	public void setbSp(List<BeliSparepartDetailModel> bSp) {
		this.bSp = bSp;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNama() {
		return nama;
	}

	public void setNama(String nama) {
		this.nama = nama;
	}

	public String getMerk() {
		return merk;
	}

	public void setMerk(String merk) {
		this.merk = merk;
	}

	public int getHarga() {
		return harga;
	}

	public void setHarga(int harga) {
		this.harga = harga;
	}
	
	
}
