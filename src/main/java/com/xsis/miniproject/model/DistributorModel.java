package com.xsis.miniproject.model;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.TableGenerator;

@Entity
@Table(name="DISTRIBUTOR")

public class DistributorModel {
	
	//property ID
	@Id
	@Column(name="ID")
	@GeneratedValue(strategy=GenerationType.TABLE, generator="DISTRIBUTOR")
	@TableGenerator(name="DISTRIBUTOR",table="SEQUENCE", pkColumnName="ID",
	pkColumnValue="DISTRIBUTOR", valueColumnName="VALUE", allocationSize=1, initialValue=1)
	private int id;
	
	@Column(name="TIPE_DISTRIBUTOR")
	private String Tdis;
	
	@Column(name="NAMA")
	private String nama;
	
	@Column(name="ALAMAT")
	private String alamat;
	
	@Column(name="KOTA")
	private String kota;
    
	@Column(name="NO_TELP")
	private String noTelp;
	
	@OneToMany(mappedBy="disMon"/*, fetch=FetchType.EAGER*/)
	private List<BeliMotorModel> beliMotor;
	
	@OneToMany(mappedBy="disSp"/*, fetch=FetchType.LAZY*/)
	/*@LazyCollection(LazyCollectionOption.FALSE)*/
	private List<BeliSparepartModel> beliSparepart;

	public List<BeliMotorModel> getBeliMotor() {
		return beliMotor;
	}

	public void setBeliMotor(List<BeliMotorModel> beliMotor) {
		this.beliMotor = beliMotor;
	}
   
	public List<BeliSparepartModel> getBeliSparepart() {
		return beliSparepart;
	}

	public void setBeliSparepart(List<BeliSparepartModel> beliSparepart) {
		this.beliSparepart = beliSparepart;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
    
	public String getTdis() {
		return Tdis;
	}

	public void setTdis(String tdis) {
		Tdis = tdis;
	}

	public String getNama() {
		return nama;
	}

	public void setNama(String nama) {
		this.nama = nama;
	}

	public String getAlamat() {
		return alamat;
	}

	public void setAlamat(String alamat) {
		this.alamat = alamat;
	}

	public String getKota() {
		return kota;
	}

	public void setKota(String kota) {
		this.kota = kota;
	}

	public String getNoTelp() {
		return noTelp;
	}

	public void setNoTelp(String noTelp) {
		this.noTelp = noTelp;
	}
}
