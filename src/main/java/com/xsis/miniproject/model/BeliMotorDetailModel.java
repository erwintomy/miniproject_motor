package com.xsis.miniproject.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.TableGenerator;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

@Entity
@Table(name="BELI_MOTOR_DETAIL")
@JsonIdentityInfo(generator=ObjectIdGenerators.IntSequenceGenerator.class, property="Id")
public class BeliMotorDetailModel {
	
	//property ID
	@Id
	@Column(name="ID")
	@GeneratedValue(strategy=GenerationType.TABLE, generator="BELI_MOTOR_DETAIL")
	@TableGenerator(name="BELI_MOTOR_DETAIL",table="SEQUENCE", pkColumnName="ID",
	pkColumnValue="BELI_MOTOR_DETAIL", valueColumnName="VALUE", allocationSize=1, initialValue=0)
	private int id;
	
	@Column(name="BELI_ID")
	private int beliId;
	
	@Column(name="MOTOR_ID")
	private int motorId;
	
	@Column(name="HARGA")
	private int harga;
    
	@Column(name="QUANTITY")
	private int quantity;

	@Column(name="SUBTOTAL")
	private int subtotal;
	
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="MOTOR_ID", insertable=false, updatable=false)
	private MotorModel bMon;

	public MotorModel getbMon() {
		return bMon;
	}

	public void setbMon(MotorModel bMon) {
		this.bMon = bMon;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getBeliId() {
		return beliId;
	}

	public void setBeliId(int beliId) {
		this.beliId = beliId;
	}

	public int getMotorId() {
		return motorId;
	}

	public void setMotorId(int motorId) {
		this.motorId = motorId;
	}

	public int getHarga() {
		return harga;
	}

	public void setHarga(int harga) {
		this.harga = harga;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public int getSubtotal() {
		return subtotal;
	}

	public void setSubtotal(int subtotal) {
		this.subtotal = subtotal;
	}
}
