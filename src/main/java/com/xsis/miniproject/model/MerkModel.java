package com.xsis.miniproject.model;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.TableGenerator;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

@Entity
@Table(name="MERK")
@JsonIdentityInfo(generator=ObjectIdGenerators.PropertyGenerator.class, property="id")
public class MerkModel {
	
	@Id
	@Column(name="ID")
	@GeneratedValue(strategy=GenerationType.TABLE, generator="MERK")
	@TableGenerator(name="MERK",table="SEQUENCE", pkColumnName="ID",
	pkColumnValue="MERK", valueColumnName="VALUE", allocationSize=1, initialValue=1)
	private int id;
	
	@Column(name="MERK_MOTOR")
	private String merk;
	
	@Column(name="ASAL_PRODUSEN")
	private String asal;

	@Column(name="THN_PRODUKSI")
	private int tahun;
	
	@OneToMany(mappedBy="merks", fetch=FetchType.EAGER)
	private List<MotorModel> motors;
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getMerk() {
		return merk;
	}

	public void setMerk(String merk) {
		this.merk = merk;
	}

	public String getAsal() {
		return asal;
	}

	public void setAsal(String asal) {
		this.asal = asal;
	}

	public int getTahun() {
		return tahun;
	}

	public void setTahun(int tahun) {
		this.tahun = tahun;
	}

	public List<MotorModel> getMotors() {
		return motors;
	}

	public void setMotors(List<MotorModel> motors) {
		this.motors = motors;
	}
}
