package com.xsis.miniproject.model;

import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

@Entity
@Table(name="BELI_SPAREPART")
@JsonIdentityInfo(generator=ObjectIdGenerators.IntSequenceGenerator.class, property="Id")
public class BeliSparepartModel {
	
	@Id
	@Column(name="ID")
	@GeneratedValue(strategy=GenerationType.TABLE, generator="BELI_SPAREPART")
	@TableGenerator(name="BELI_SPAREPART",table="SEQUENCE", pkColumnName="ID",
	pkColumnValue="BELI_SPAREPART", valueColumnName="VALUE", allocationSize=1, initialValue=1)
	private int id;
	
	@Column(name="NO_FAKTUR")
	private String noFaktur;
	
	@DateTimeFormat(pattern="yyyy-MM-dd")
	@Temporal(TemporalType.DATE)
	@Column(name="TGL_BELI")
	private Date tglBeli;

	@Column(name="DISTRIBUTOR_ID")
	private int distributorId;

	@Column(name="TOTAL")
	private int total;
	
	@Transient
	private List<BeliSparepartDetailModel> detail;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "DISTRIBUTOR_ID", insertable = false, updatable = false)
	private DistributorModel disSp;
	
	public DistributorModel getDisSp() {
		return disSp;
	}

	public void setDisSp(DistributorModel disSp) {
		this.disSp = disSp;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNoFaktur() {
		return noFaktur;
	}

	public void setNoFaktur(String noFaktur) {
		this.noFaktur = noFaktur;
	}

	public Date getTglBeli() {
		return tglBeli;
	}

	public void setTglBeli(Date tglBeli) {
		this.tglBeli = tglBeli;
	}

	public int getDistributorId() {
		return distributorId;
	}

	public void setDistributorId(int distributorId) {
		this.distributorId = distributorId;
	}

	public int getTotal() {
		return total;
	}

	public void setTotal(int total) {
		this.total = total;
	}

	public List<BeliSparepartDetailModel> getDetail() {
		return detail;
	}

	public void setDetail(List<BeliSparepartDetailModel> detail) {
		this.detail = detail;
	}

}
