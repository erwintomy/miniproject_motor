package com.xsis.miniproject.model;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.TableGenerator;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

@Entity
@Table(name="PELANGGAN")
@JsonIdentityInfo(generator=ObjectIdGenerators.IntSequenceGenerator.class, property="Id")
public class PelangganModel {
	
	//property ID
	@Id
	@Column(name="ID")
	@GeneratedValue(strategy=GenerationType.TABLE, generator="PELANGGAN")
	@TableGenerator(name="PELANGGAN",table="SEQUENCE", pkColumnName="ID",
	pkColumnValue="PELANGGAN", valueColumnName="VALUE", allocationSize=1, initialValue=1)
	private int id;
	
	@Column(name="TIPE_PELANGGAN")
	private String Tpel;
	
	@Column(name="NAMA")
	private String nama;
	
	@Column(name="ALAMAT")
	private String alamat;
	
	@Column(name="GENDER")
	private String gender;
	
	@Column(name="NO_KTP")
	private String noKtp;
	
	@Column(name="KOTA")
	private String kota;
    
	@Column(name="NO_TELP")
	private String noTelp;

	@OneToMany(mappedBy="pelMon"/*, fetch=FetchType.EAGER*/)
	private List<JualMotorModel> jualMotor;
	
	@OneToMany(mappedBy="pelSp"/*, fetch=FetchType.LAZY*/)
	/*@LazyCollection(LazyCollectionOption.FALSE)*/
	private List<JualSparepartModel> jualSparepart;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getTpel() {
		return Tpel;
	}

	public void setTpel(String tpel) {
		Tpel = tpel;
	}

	public String getNama() {
		return nama;
	}

	public void setNama(String nama) {
		this.nama = nama;
	}

	public String getAlamat() {
		return alamat;
	}

	public void setAlamat(String alamat) {
		this.alamat = alamat;
	}
    
	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getNoKtp() {
		return noKtp;
	}

	public void setNoKtp(String noKtp) {
		this.noKtp = noKtp;
	}

	public String getKota() {
		return kota;
	}

	public void setKota(String kota) {
		this.kota = kota;
	}
	
	public String getNoTelp() {
		return noTelp;
	}

	public void setNoTelp(String noTelp) {
		this.noTelp = noTelp;
	}

	public List<JualMotorModel> getJualMotor() {
		return jualMotor;
	}

	public void setJualMotor(List<JualMotorModel> jualMotor) {
		this.jualMotor = jualMotor;
	}

	public List<JualSparepartModel> getJualSparepart() {
		return jualSparepart;
	}

	public void setJualSparepart(List<JualSparepartModel> jualSparepart) {
		this.jualSparepart = jualSparepart;
	}
}
