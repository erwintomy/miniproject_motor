package com.xsis.miniproject.model;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.TableGenerator;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

@Entity
@Table(name="MOTOR")
@JsonIdentityInfo(generator=ObjectIdGenerators.PropertyGenerator.class, property="id")
public class MotorModel {
	
	@Id
	@Column(name="ID")
	@GeneratedValue(strategy=GenerationType.TABLE, generator="MOTOR")
	@TableGenerator(name="MOTOR",table="SEQUENCE", pkColumnName="ID",
	pkColumnValue="MOTOR", valueColumnName="VALUE", allocationSize=1, initialValue=1)
	private int id;
	
	@Column(name="NAMA")
	private String nama;
	
	@Column(name="MERK_ID")
	private int merkId;
	
	@Column(name="NO_RANGKA")
	private String noRangka;
	
	@Column(name="DESKRIPSI")
	private String deskripsi;	

	@Column(name="STOK")
	private int stok;
	
	@Column(name="HARGA")
	private int harga;
	
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="MERK_ID",insertable=false, updatable=false)
	private MerkModel merks;
	
	@OneToMany(mappedBy="jMon" /*, fetch=FetchType.EAGER*/)
	private List<JualMotorDetailModel> jMon;
	
	@OneToMany(mappedBy="bMon"/*, fetch=FetchType.LAZY*/)
	/*@LazyCollection(LazyCollectionOption.FALSE)*/
	private List<BeliMotorDetailModel> bMon;
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNama() {
		return nama;
	}

	public void setNama(String nama) {
		this.nama = nama;
	}

	public int getMerkId() {
		return merkId;
	}

	public void setMerkId(int merkId) {
		this.merkId = merkId;
	}

	public int getHarga() {
		return harga;
	}

	public void setHarga(int harga) {
		this.harga = harga;
	}

	public String getNoRangka() {
		return noRangka;
	}

	public void setNoRangka(String noRangka) {
		this.noRangka = noRangka;
	}

	public String getDeskripsi() {
		return deskripsi;
	}

	public void setDeskripsi(String deskripsi) {
		this.deskripsi = deskripsi;
	}

	public MerkModel getMerks() {
		return merks;
	}

	public void setMerks(MerkModel merks) {
		this.merks = merks;
	}

	public List<JualMotorDetailModel> getjMon() {
		return jMon;
	}

	public void setjMon(List<JualMotorDetailModel> jMon) {
		this.jMon = jMon;
	}

	public List<BeliMotorDetailModel> getbMon() {
		return bMon;
	}

	public void setbMon(List<BeliMotorDetailModel> bMon) {
		this.bMon = bMon;
	}

	public int getStok() {
		return stok;
	}

	public void setStok(int stok) {
		this.stok = stok;
	}
	
}
