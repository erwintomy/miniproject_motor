package com.xsis.miniproject.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.TableGenerator;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

@Entity
@Table(name="JUAL_SPAREPART_DETAIL")
@JsonIdentityInfo(generator=ObjectIdGenerators.IntSequenceGenerator.class, property="Id")
public class JualSparepartDetailModel {
	
	//property ID
	@Id
	@Column(name="ID")
	@GeneratedValue(strategy=GenerationType.TABLE, generator="JUAL_SPAREPART_DETAIL")
	@TableGenerator(name="JUAL_SPAREPART_DETAIL",table="SEQUENCE", pkColumnName="ID",
	pkColumnValue="JUAL_SPAREPART_DETAIL", valueColumnName="VALUE", allocationSize=1, initialValue=0)
	private int id;
	
	@Column(name="JUAL_ID")
	private int jualId;
	
	@Column(name="SPAREPART_ID")
	private int sparepartId;
	
	@Column(name="HARGA")
	private int harga;
    
	@Column(name="QUANTITY")
	private int quantity;

	@Column(name="SUBTOTAL")
	private int subtotal;
	
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="SPAREPART_ID", insertable=false, updatable=false)
	private SparepartModel jSp;

	public SparepartModel getjSp() {
		return jSp;
	}

	public void setjSp(SparepartModel jSp) {
		this.jSp = jSp;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getJualId() {
		return jualId;
	}

	public void setJualId(int jualId) {
		this.jualId = jualId;
	}

	public int getSparepartId() {
		return sparepartId;
	}

	public void setSparepartId(int sparepartId) {
		this.sparepartId = sparepartId;
	}

	public int getHarga() {
		return harga;
	}

	public void setHarga(int harga) {
		this.harga = harga;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public int getSubtotal() {
		return subtotal;
	}

	public void setSubtotal(int subtotal) {
		this.subtotal = subtotal;
	}
}
