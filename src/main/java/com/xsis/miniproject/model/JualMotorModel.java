package com.xsis.miniproject.model;

import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

@Entity
@Table(name="JUAL_MOTOR")
@JsonIdentityInfo(generator=ObjectIdGenerators.IntSequenceGenerator.class, property="Id")
public class JualMotorModel {
	
	//property ID
	@Id
	@Column(name="ID")
	@GeneratedValue(strategy=GenerationType.TABLE, generator="JUAL_MOTOR")
	@TableGenerator(name="JUAL_MOTOR",table="SEQUENCE", pkColumnName="ID",
	pkColumnValue="JUAL_MOTOR", valueColumnName="VALUE", allocationSize=1, initialValue=1)
	private int id;
	
	@Column(name="NO_STRUK")
	private String noStruk;
	
	@Column(name="PELANGGAN_ID")
	private int pelangganId;
	
	@DateTimeFormat(pattern="yyyy-MM-dd")
	@Temporal(TemporalType.DATE)
	@Column(name="TGLJUAL")
	private Date tglJual;
		
	@Column(name="TOTAL")
	private int total;
	
	@Transient //Tidak ditambahkan ke kolom tabel
	private List<JualMotorDetailModel> detail;
	
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="PELANGGAN_ID", insertable=false, updatable=false)
	private PelangganModel pelMon;

	public PelangganModel getPelMon() {
		return pelMon;
	}

	public void setPelMon(PelangganModel pelMon) {
		this.pelMon = pelMon;
	}

	public List<JualMotorDetailModel> getDetail() {
		return detail;
	}

	public void setDetail(List<JualMotorDetailModel> detail) {
		this.detail = detail;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNoStruk() {
		return noStruk;
	}

	public void setNoStruk(String noStruk) {
		this.noStruk = noStruk;
	}

	public int getPelangganId() {
		return pelangganId;
	}

	public void setPelangganId(int pelangganId) {
		this.pelangganId = pelangganId;
	}

	public Date getTglJual() {
		return tglJual;
	}

	public void setTglJual(Date tglJual) {
		this.tglJual = tglJual;
	}

	public int getTotal() {
		return total;
	}

	public void setTotal(int total) {
		this.total = total;
	}
}
