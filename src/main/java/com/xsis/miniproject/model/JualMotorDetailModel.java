package com.xsis.miniproject.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.TableGenerator;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

@Entity
@Table(name="JUAL_MOTOR_DETAIL")
@JsonIdentityInfo(generator=ObjectIdGenerators.IntSequenceGenerator.class, property="Id")
public class JualMotorDetailModel {
	
	//property ID
	@Id
	@Column(name="ID")
	@GeneratedValue(strategy=GenerationType.TABLE, generator="JUAL_MOTOR_DETAIL")
	@TableGenerator(name="JUAL_MOTOR_DETAIL",table="SEQUENCE", pkColumnName="ID",
	pkColumnValue="JUAL_MOTOR_DETAIL", valueColumnName="VALUE", allocationSize=1, initialValue=0)
	private int id;
	
	@Column(name="JUAL_ID")
	private int jualId;
	
	@Column(name="MOTOR_ID")
	private int motorId;
	
	@Column(name="HARGA")
	private int harga;
    
	@Column(name="QUANTITY")
	private int quantity;

	@Column(name="SUBTOTAL")
	private int subtotal;

	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="MOTOR_ID", insertable=false, updatable=false)
	private MotorModel jMon;

	public MotorModel getjMon() {
		return jMon;
	}

	public void setjMon(MotorModel jMon) {
		this.jMon = jMon;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getJualId() {
		return jualId;
	}

	public void setJualId(int jualId) {
		this.jualId = jualId;
	}

	public int getMotorId() {
		return motorId;
	}

	public void setMotorId(int motorId) {
		this.motorId = motorId;
	}

	public int getHarga() {
		return harga;
	}

	public void setHarga(int harga) {
		this.harga = harga;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public int getSubtotal() {
		return subtotal;
	}

	public void setSubtotal(int subtotal) {
		this.subtotal = subtotal;
	}
}
