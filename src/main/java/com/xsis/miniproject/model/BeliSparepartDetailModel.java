package com.xsis.miniproject.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.TableGenerator;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

@Entity
@Table(name="BELI_SPAREPART_DETAIL")
@JsonIdentityInfo(generator=ObjectIdGenerators.IntSequenceGenerator.class, property="Id")
public class BeliSparepartDetailModel {
	
	@Id
	@Column(name="ID")
	@GeneratedValue(strategy=GenerationType.TABLE, generator="BELI_SPAREPART_DETAIL")
	@TableGenerator(name="BELI_SPAREPART_DETAIL",table="SEQUENCE", pkColumnName="ID",
	pkColumnValue="BELI_SPAREPART_DETAIL", valueColumnName="VALUE", allocationSize=1, initialValue=1)
	private int id;
	
	@Column(name="SPAREPART_ID")
	private int sparepartId;
	
	@Column(name="BELI_ID")
	private int beliId;
	
	@Column(name="HARGA")
	private int harga;

	@Column(name="QUANTITY")
	private int qty;

	@Column(name="SUBTOTAL")
	private int subtotal;

	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="SPAREPART_ID", insertable=false, updatable=false)
	private SparepartModel bSp;

	public SparepartModel getbSp() {
		return bSp;
	}

	public void setbSp(SparepartModel bSp) {
		this.bSp = bSp;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
	
	public int getSparepartId() {
		return sparepartId;
	}

	public void setSparepartId(int sparepartId) {
		this.sparepartId = sparepartId;
	}

	public int getBeliId() {
		return beliId;
	}

	public void setBeliId(int beliId) {
		this.beliId = beliId;
	}

	public int getHarga() {
		return harga;
	}

	public void setHarga(int harga) {
		this.harga = harga;
	}

	public int getQty() {
		return qty;
	}

	public void setQty(int qty) {
		this.qty = qty;
	}

	public int getSubtotal() {
		return subtotal;
	}

	public void setSubtotal(int subtotal) {
		this.subtotal = subtotal;
	}
		
}
