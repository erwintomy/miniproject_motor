package com.xsis.miniproject.dao.impl;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.xsis.miniproject.dao.MotorDao;
import com.xsis.miniproject.model.MotorModel;

@Repository
public class MotorDaoImpl implements MotorDao {
	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public List<MotorModel> get() throws Exception {
		Session session = this.sessionFactory.getCurrentSession();
		List<MotorModel> result = session.createQuery("select a from MotorModel as a").list();
		return result;
	}

	@Override
	public void insert(MotorModel model) throws Exception {
		Session session = this.sessionFactory.getCurrentSession();
		session.save(model);
	}

	@Override
	public MotorModel getById(int id) throws Exception {
		Session session = this.sessionFactory.getCurrentSession();
		MotorModel result = session.get(MotorModel.class, id);
        return result;
	}

	@Override
	public void update(MotorModel model) throws Exception {
		Session session = this.sessionFactory.getCurrentSession();
		session.update(model);
	}

	@Override
	public void delete(MotorModel model) throws Exception {
		Session session = this.sessionFactory.getCurrentSession();
		session.delete(model);

	}
	
	@Override
	public void save(MotorModel model) throws Exception {
		Session session = this.sessionFactory.getCurrentSession();
		session.save(model);

	}

	@Override
	public int getByNama(String nama) throws Exception {
		Session session = this.sessionFactory.getCurrentSession();
		/*pdo = prosedur query objek (:nama)*/
		String hql = "select x from MotorModel as x where x.nama = :nama";
		Query query = session.createQuery(hql);
		query.setParameter("nama", nama);
		
		int result = 0;
		if(query.list().size()>0){
			result = 1;
		}
		return result;
	}

	@Override
	public int getByNoRangka(String noRangka) throws Exception {
		Session session = this.sessionFactory.getCurrentSession();

		String hql = "select x from MotorModel as x where x.noRangka = :noRangka";
		Query query = session.createQuery(hql);
		query.setParameter("noRangka", noRangka);
		
		int result = 0;
		if(query.list().size()>0){
			result = 1;
		}
		return result;
	}
}
