package com.xsis.miniproject.dao.impl;

import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.xsis.miniproject.dao.LoginDao;
import com.xsis.miniproject.model.UsersModel;

@Repository
public class LoginDaoImpl implements LoginDao {
	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public List<UsersModel> get() throws Exception {
		Session session = this.sessionFactory.getCurrentSession();
		List<UsersModel> result = session.createQuery("select a from UsersModel as a").list();
		return result;
	}	

	@Override
	public UsersModel getById(int id) throws Exception {
		Session session = this.sessionFactory.getCurrentSession();
		UsersModel result = session.get(UsersModel.class, id);
        return result;
	}

	@Override
	public void update(UsersModel model) throws Exception {
		Session session = this.sessionFactory.getCurrentSession();
		session.update(model);
	}

	@Override
	public void delete(UsersModel model) throws Exception {
		Session session = this.sessionFactory.getCurrentSession();
		session.delete(model);

	}
	
	@Override
	public void save(UsersModel model) throws Exception {
		Session session = this.sessionFactory.getCurrentSession();
		session.save(model);
	}
	
	@Override
	public void insert(UsersModel model) throws Exception {
		Session session = this.sessionFactory.getCurrentSession();
		session.save(model);
	}
}
