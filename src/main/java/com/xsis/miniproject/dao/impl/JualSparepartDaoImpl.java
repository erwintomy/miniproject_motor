package com.xsis.miniproject.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.xsis.miniproject.dao.JualSparepartDao;
import com.xsis.miniproject.model.JualSparepartDetailModel;
import com.xsis.miniproject.model.JualSparepartModel;
import com.xsis.miniproject.model.SparepartModel;

@Repository
public class JualSparepartDaoImpl implements JualSparepartDao {
	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public List<JualSparepartModel> get() throws Exception {
		Session session = this.sessionFactory.getCurrentSession();
		List<JualSparepartModel> result = session.createQuery("select fak from JualSparepartModel as fak").list();
		return result;
	}

	@Override
	public void insert(JualSparepartModel model) throws Exception {
		Session session = this.sessionFactory.getCurrentSession();
		session.save(model);
	}

	@Override
	public JualSparepartModel getById(int id) throws Exception {
		Session session = this.sessionFactory.getCurrentSession();
		JualSparepartModel result = session.get(JualSparepartModel.class, id);

		List<JualSparepartDetailModel> detail = new ArrayList<JualSparepartDetailModel>();
		Criteria criteria = session.createCriteria(JualSparepartDetailModel.class);
		criteria.add(Restrictions.eq("jualId", id));
		// detail diisi dengan criteria
		detail = criteria.list();
		// memasukkan detail ke result
		result.setDetail(detail);
		/*session.createQuery("select detail from BeliModel where id=:id");*/
		
		return result;
	}

	@Override
	public void update(JualSparepartModel model) throws Exception {
		Session session = this.sessionFactory.getCurrentSession();
		session.update(model);
		
		String hql= "from JualSparepartDetailModel where jualId = :id";
		Query query = session.createQuery(hql);
		query.setInteger("id", model.getId());
		//query.executeUpdate();
		List<JualSparepartDetailModel> listDetail = query.list();
		
			for (JualSparepartDetailModel detail : listDetail){
				SparepartModel sparepart = session.get(SparepartModel.class, detail.getSparepartId());
				// simpan data ke database
				sparepart.setStok(sparepart.getStok()+detail.getQuantity());
				session.update(sparepart);
				session.delete(detail);
			}
		
			if(model.getDetail().size() > 0){
				for (JualSparepartDetailModel item : model.getDetail()){
					item.setJualId(model.getId());
					session.save(item);
					//ambil barang
					SparepartModel sparepart = session.get(SparepartModel.class, item.getSparepartId());
					// kurangi stok
					sparepart.setStok(sparepart.getStok()-item.getQuantity());
					session.update(sparepart);
					
				}
			}
	}

	@Override
	public void delete(JualSparepartModel model) throws Exception {
		Session session = this.sessionFactory.getCurrentSession();
		String hql= "from JualSparepartDetailModel where jualId = :id";
		Query query = session.createQuery(hql);
		query.setInteger("id", model.getId());
		
		List<JualSparepartDetailModel> listDetail=query.list();
			
			for (JualSparepartDetailModel detail : listDetail){
				/*session.clear();*/
				Session session2 = this.sessionFactory.getCurrentSession();
				
				//ambil barang
				SparepartModel sparepart = session2.get(SparepartModel.class, detail.getSparepartId());
				// kurangi stok
	
				int qty = detail.getQuantity();
				int stok = sparepart.getStok();
				sparepart.setStok(stok + qty);
				session2.update(sparepart);
				session2.delete(detail);	
			}
				
		session.delete(model);
	}
	
	@Override
	public void save(JualSparepartModel model) throws Exception {
		Session session = this.sessionFactory.getCurrentSession();
		session.save(model);
		
		List<JualSparepartDetailModel> items = model.getDetail();
		if(items.size() > 0){
			for (JualSparepartDetailModel item : items){
				item.setJualId(model.getId());
				session.save(item);
				//ambil barang
				SparepartModel sparepart = session.get(SparepartModel.class, item.getSparepartId());
				// kurangi stok
				sparepart.setStok(sparepart.getStok()-item.getQuantity());
				session.update(sparepart);
				
			}
		}

	}
	
	@Override
	public int getByNo(String noStruk) throws Exception {
		Session session = this.sessionFactory.getCurrentSession();

		String hql = "select x from JualSparepartModel as x where x.noStruk = :noStruk";
		Query query = session.createQuery(hql);
		query.setParameter("noStruk", noStruk);
		
		int result = 0;
		if(query.list().size()>0){
			result = 1;
		}
		return result;
	}

}
