package com.xsis.miniproject.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.xsis.miniproject.dao.JualMotorDao;
import com.xsis.miniproject.model.JualMotorDetailModel;
import com.xsis.miniproject.model.JualMotorModel;
import com.xsis.miniproject.model.JualMotorDetailModel;
import com.xsis.miniproject.model.MotorModel;
import com.xsis.miniproject.model.MotorModel;
import com.xsis.miniproject.model.MotorModel;

@Repository
public class JualMotorDaoImpl implements JualMotorDao {
	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public List<JualMotorModel> get() throws Exception {
		Session session = this.sessionFactory.getCurrentSession();
		List<JualMotorModel> result = session.createQuery("select a from JualMotorModel as a").list();
		return result;
	}

	@Override
	public void insert(JualMotorModel model) throws Exception {
		Session session = this.sessionFactory.getCurrentSession();
		session.save(model);
	}

	@Override
	public JualMotorModel getById(int id) throws Exception {
		Session session = this.sessionFactory.getCurrentSession();
		JualMotorModel result = session.get(JualMotorModel.class, id);

		List<JualMotorDetailModel> detail = new ArrayList<JualMotorDetailModel>();
		Criteria criteria = session.createCriteria(JualMotorDetailModel.class);
		criteria.add(Restrictions.eq("jualId", id));
		// detail diisi dengan criteria
		detail = criteria.list();
		// memasukkan detail ke result
		result.setDetail(detail);
		/*session.createQuery("select detail from BeliModel where id=:id");*/
		
		return result;
	}

	@Override
	public void update(JualMotorModel model) throws Exception {
		Session session = this.sessionFactory.getCurrentSession();
		session.update(model);
		
		String hql= "from JualMotorDetailModel where jualId = :id";
		Query query = session.createQuery(hql);
		query.setInteger("id", model.getId());
		/*query.executeUpdate();*/
		
		List<JualMotorDetailModel> listDetail = query.list();
		
		for (JualMotorDetailModel detail : listDetail){
			MotorModel motor = session.get(MotorModel.class, detail.getMotorId());
			// simpan data ke database
			motor.setStok(motor.getStok()+detail.getQuantity());
			session.update(motor);
			session.delete(detail);
		}
		// input data baru seperti insert
		if(model.getDetail().size() > 0){
			for (JualMotorDetailModel item : model.getDetail()){
				item.setJualId(model.getId());
				// simpan data ke database
				session.save(item);
				//ambil barang
				MotorModel motor = session.get(MotorModel.class, item.getMotorId());
				// kurangi stok
				motor.setStok(motor.getStok()-item.getQuantity());
				session.update(motor);
			}
		}
	}

	@Override
	public void delete(JualMotorModel model) throws Exception {
		Session session = this.sessionFactory.getCurrentSession();
		String hql= "from JualMotorDetailModel where jualId = :id";
		Query query = session.createQuery(hql);
		query.setInteger("id", model.getId());
		
		List<JualMotorDetailModel> listDetail=query.list();
		
		for (JualMotorDetailModel detail : listDetail){
			
			/*session.clear();*/
			Session session2 = this.sessionFactory.getCurrentSession();
			
			//ambil barang
			MotorModel motor = session2.get(MotorModel.class, detail.getMotorId());
			// kurangi stok

			int qty = detail.getQuantity();
			int stok = motor.getStok();
			motor.setStok(stok + qty);
			session2.update(motor);
			session2.delete(detail);	
		}
		session.delete(model);

	}
	
	@Override
	public void save(JualMotorModel model) throws Exception {
		Session session = this.sessionFactory.getCurrentSession();
		session.save(model);
		
		List<JualMotorDetailModel> items = model.getDetail();
		if(items.size() > 0){
			for (JualMotorDetailModel item : items){
				
				item.setJualId(model.getId());
				session.save(item);
				MotorModel motor = session.get(MotorModel.class, item.getMotorId());
				// kurangi stok
				motor.setStok(motor.getStok()-item.getQuantity());
				session.update(motor);
			}
		}

	}

	@Override
	public int getByNoStruk(String noStruk) throws Exception {
		Session session = this.sessionFactory.getCurrentSession();

		String hql = "select x from JualMotorModel as x where x.noStruk = :noStruk";
		Query query = session.createQuery(hql);
		query.setParameter("noStruk", noStruk);
		
		int result = 0;
		if(query.list().size()>0){
			result = 1;
		}
		return result;
	}
}
