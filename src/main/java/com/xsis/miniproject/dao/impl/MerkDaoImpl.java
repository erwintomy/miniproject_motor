package com.xsis.miniproject.dao.impl;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.xsis.miniproject.dao.MerkDao;
import com.xsis.miniproject.model.MerkModel;

@Repository
public class MerkDaoImpl implements MerkDao {
	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public List<MerkModel> get() throws Exception {
		Session session = this.sessionFactory.getCurrentSession();
		List<MerkModel> result = session.createQuery("select a from MerkModel as a").list();
		return result;
	}

	@Override
	public void insert(MerkModel model) throws Exception {
		Session session = this.sessionFactory.getCurrentSession();
		session.save(model);
	}

	@Override
	public MerkModel getById(int id) throws Exception {
		Session session = this.sessionFactory.getCurrentSession();
		MerkModel result = session.get(MerkModel.class, id);
        return result;
	}

	@Override
	public void update(MerkModel model) throws Exception {
		Session session = this.sessionFactory.getCurrentSession();
		session.update(model);
	}

	@Override
	public void delete(MerkModel model) throws Exception {
		Session session = this.sessionFactory.getCurrentSession();
		session.delete(model);

	}
	
	@Override
	public void save(MerkModel model) throws Exception {
		Session session = this.sessionFactory.getCurrentSession();
		session.save(model);

	}
	
	@Override
	public int getByNamaMerk(String merk) throws Exception {
		Session session = this.sessionFactory.getCurrentSession();

		String hql = "select x from MerkModel as x where x.merk = :merk";
		Query query = session.createQuery(hql);
		query.setParameter("merk", merk);
		
		int result = 0;
		if(query.list().size()>0){
			result = 1;
		}
		return result;
	}
}
