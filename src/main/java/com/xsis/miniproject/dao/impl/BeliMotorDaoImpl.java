package com.xsis.miniproject.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.xsis.miniproject.dao.BeliMotorDao;
import com.xsis.miniproject.model.BeliMotorDetailModel;
import com.xsis.miniproject.model.BeliMotorModel;
import com.xsis.miniproject.model.BeliMotorDetailModel;
import com.xsis.miniproject.model.BeliMotorDetailModel;
import com.xsis.miniproject.model.MotorModel;

@Repository
public class BeliMotorDaoImpl implements BeliMotorDao {
	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public List<BeliMotorModel> get() throws Exception {
		Session session = this.sessionFactory.getCurrentSession();
		List<BeliMotorModel> result = session.createQuery("select a from BeliMotorModel as a").list();
		return result;
	}

	@Override
	public void insert(BeliMotorModel model) throws Exception {
		Session session = this.sessionFactory.getCurrentSession();
		session.save(model);
	}

	@Override
	public BeliMotorModel getById(int id) throws Exception {
		Session session = this.sessionFactory.getCurrentSession();
		BeliMotorModel result = session.get(BeliMotorModel.class, id);

		List<BeliMotorDetailModel> detail = new ArrayList<BeliMotorDetailModel>();
		Criteria criteria = session.createCriteria(BeliMotorDetailModel.class);
		criteria.add(Restrictions.eq("beliId", id));
		// detail diisi dengan criteria
		detail = criteria.list();
		// memasukkan detail ke result
		result.setDetail(detail);
		/*session.createQuery("select detail from BeliMotorModel where id=:id");
		*/
		return result;
	}
	
	@Override
	public int getByNo(String noFaktur) throws Exception {
		Session session = this.sessionFactory.getCurrentSession();
		String hql ="select x from BeliMotorModel as x where x.noFaktur = :noFaktur";
		Query query = session.createQuery(hql);
		query.setParameter("noFaktur", noFaktur);
		
		int result = 0;
		if (query.list().size()>0){
			result = 1;
		}
		return result;
	}
	

	@Override
	public void update(BeliMotorModel model) throws Exception {
		Session session = this.sessionFactory.getCurrentSession();
		session.update(model);
		
		String hql= "from BeliMotorDetailModel where beliId = :id";
		Query query = session.createQuery(hql);
		query.setInteger("id", model.getId());
		/*query.executeUpdate();*/
		
        List<BeliMotorDetailModel> listDetail = query.list();
		
		for (BeliMotorDetailModel detail : listDetail){
			MotorModel motor = session.get(MotorModel.class, detail.getMotorId());
			// simpan data ke database
			motor.setStok(motor.getStok()-detail.getQuantity());
			session.update(motor);
			session.delete(detail);
		}
		
		// input data baru seperti insert
		if(model.getDetail().size() > 0){
			for (BeliMotorDetailModel item : model.getDetail()){
				item.setBeliId(model.getId());
				// simpan data ke database
				session.save(item);
				MotorModel motor = session.get(MotorModel.class, item.getMotorId());
				// kurangi stok
				motor.setStok(motor.getStok()+item.getQuantity());
				session.update(motor);
			}
		}
	}

	@Override
	public void delete(BeliMotorModel model) throws Exception {
		Session session = this.sessionFactory.getCurrentSession();
		String hql= "from BeliMotorDetailModel where beliId = :id";
		Query query = session.createQuery(hql);
		query.setInteger("id", model.getId());
		/*query.executeUpdate();*/
		
        List<BeliMotorDetailModel> listDetail=query.list();
		
		for (BeliMotorDetailModel detail : listDetail){
			
			/*session.clear();*/
			Session session2 = this.sessionFactory.getCurrentSession();
			
			//ambil barang
			MotorModel motor = session2.get(MotorModel.class, detail.getMotorId());
			// kurangi stok

			int qty = detail.getQuantity();
			int stok = motor.getStok();
			motor.setStok(stok - qty);
			session2.update(motor);
			session2.delete(detail);	
		}
		session.delete(model);

	}
	
	@Override
	public void save(BeliMotorModel model) throws Exception {
		Session session = this.sessionFactory.getCurrentSession();
		session.save(model);
		
		List<BeliMotorDetailModel> items = model.getDetail();
		if(items.size() > 0){
			for (BeliMotorDetailModel item : items){
				
				item.setBeliId(model.getId());
				session.save(item);
				MotorModel motor = session.get(MotorModel.class, item.getMotorId());
				// kurangi stok
				motor.setStok(motor.getStok()+item.getQuantity());
				session.update(motor);
			}
		}

	}
	

}
