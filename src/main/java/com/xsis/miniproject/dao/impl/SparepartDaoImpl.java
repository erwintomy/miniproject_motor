package com.xsis.miniproject.dao.impl;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.xsis.miniproject.dao.SparepartDao;
import com.xsis.miniproject.model.SparepartModel;

@Repository
public class SparepartDaoImpl implements SparepartDao {
	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public List<SparepartModel> get() throws Exception {
		Session session = this.sessionFactory.getCurrentSession();
		List<SparepartModel> result = session.createQuery("select a from SparepartModel as a").list();
		return result;
	}

	@Override
	public void insert(SparepartModel model) throws Exception {
		Session session = this.sessionFactory.getCurrentSession();
		session.save(model);
	}

	@Override
	public SparepartModel getById(int id) throws Exception {
		Session session = this.sessionFactory.getCurrentSession();
		SparepartModel result = session.get(SparepartModel.class, id);
        return result;
	}

	@Override
	public void update(SparepartModel model) throws Exception {
		Session session = this.sessionFactory.getCurrentSession();
		session.update(model);
	}

	@Override
	public void delete(SparepartModel model) throws Exception {
		Session session = this.sessionFactory.getCurrentSession();
		session.delete(model);

	}
	
	@Override
	public void save(SparepartModel model) throws Exception {
		Session session = this.sessionFactory.getCurrentSession();
		session.save(model);

	}

	
	@Override
	public int getByName(String nama) throws Exception {
		Session session = this.sessionFactory.getCurrentSession();
		String hql ="select x from SparepartModel as x where x.nama = :nama";
		Query query = session.createQuery(hql);
		query.setParameter("nama", nama);
		
		int result = 0;
		if (query.list().size()>0){
			result = 1;
		}
		return result;
	}
	

}
