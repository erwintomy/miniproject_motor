package com.xsis.miniproject.dao.impl;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.xsis.miniproject.dao.DistributorDao;
import com.xsis.miniproject.model.DistributorModel;
import com.xsis.miniproject.model.PelangganModel;

@Repository
public class DistributorDaoImpl implements DistributorDao {
	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public List<DistributorModel> get() throws Exception {
		Session session = this.sessionFactory.getCurrentSession();
		List<DistributorModel> result = session.createQuery("select a from DistributorModel as a").list();
		return result;
	}

	@Override
	public void insert(DistributorModel model) throws Exception {
		Session session = this.sessionFactory.getCurrentSession();
		session.save(model);
	}

	@Override
	public DistributorModel getById(int id) throws Exception {
		Session session = this.sessionFactory.getCurrentSession();
		DistributorModel result = session.get(DistributorModel.class, id);
        return result;
	}

	@Override
	public void update(DistributorModel model) throws Exception {
		Session session = this.sessionFactory.getCurrentSession();
		session.update(model);
	}

	@Override
	public void delete(DistributorModel model) throws Exception {
		Session session = this.sessionFactory.getCurrentSession();
		session.delete(model);

	}
	
	@Override
	public void save(DistributorModel model) throws Exception {
		Session session = this.sessionFactory.getCurrentSession();
		session.save(model);

	}
	
	@Override
	public int getByName(String nama) throws Exception {
		Session session = this.sessionFactory.getCurrentSession();
		String hql ="select x from DistributorModel as x where x.nama = :nama";
		Query query = session.createQuery(hql);
		query.setParameter("nama", nama);
		
		int result = 0;
		if (query.list().size()>0){
			result = 1;
		}
		return result;
	}
    
	@Override
	public List<DistributorModel> getByTdisMon() throws Exception {
		Session session = this.sessionFactory.getCurrentSession();
		List<DistributorModel> result = session.createQuery("select a from DistributorModel as a where a.Tdis like 'Motor' ").list();
		return result;
	}
	
	@Override
	public List<DistributorModel> getByTdisSp() throws Exception {
		Session session = this.sessionFactory.getCurrentSession();
		List<DistributorModel> result = session.createQuery("select a from DistributorModel as a where a.Tdis like 'Sparepart' ").list();
		return result;
	}
}
