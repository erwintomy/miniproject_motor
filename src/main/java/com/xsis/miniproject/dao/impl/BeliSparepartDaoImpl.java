package com.xsis.miniproject.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.xsis.miniproject.dao.BeliSparepartDao;
import com.xsis.miniproject.model.BeliSparepartDetailModel;
import com.xsis.miniproject.model.BeliSparepartModel;
import com.xsis.miniproject.model.JualSparepartDetailModel;
import com.xsis.miniproject.model.SparepartModel;

@Repository
public class BeliSparepartDaoImpl implements BeliSparepartDao {
	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public List<BeliSparepartModel> get() throws Exception {
		Session session = this.sessionFactory.getCurrentSession();
		List<BeliSparepartModel> result = session.createQuery("select a from BeliSparepartModel as a").list();
		return result;
	}

	@Override
	public void insert(BeliSparepartModel model) throws Exception {
		Session session = this.sessionFactory.getCurrentSession();
		session.save(model);
	}

	@Override
	public BeliSparepartModel getById(int id) throws Exception {
		Session session = this.sessionFactory.getCurrentSession();
		BeliSparepartModel result = session.get(BeliSparepartModel.class, id);

		List<BeliSparepartDetailModel> detail = new ArrayList<BeliSparepartDetailModel>();
		Criteria criteria = session.createCriteria(BeliSparepartDetailModel.class);
		criteria.add(Restrictions.eq("beliId", id));
		// detail diisi dengan criteria
		detail = criteria.list();
		// memasukkan detail ke result
		result.setDetail(detail);
		/*session.createQuery("select detail from BeliModel where id=:id");*/
		
		return result;
	}
	
	@Override
	public int getByNo(String noFaktur) throws Exception {
		Session session = this.sessionFactory.getCurrentSession();
		String hql ="select x from BeliSparepartModel as x where x.noFaktur = :noFaktur";
		Query query = session.createQuery(hql);
		query.setParameter("noFaktur", noFaktur);
		
		int result = 0;
		if (query.list().size()>0){
			result = 1;
		}
		return result;
	}
	

	@Override
	public void update(BeliSparepartModel model) throws Exception {
		Session session = this.sessionFactory.getCurrentSession();
		session.update(model);
		
		String hql= "from BeliSparepartDetailModel where beliId = :id";
		Query query = session.createQuery(hql);
		query.setInteger("id", model.getId());
		
		List<BeliSparepartDetailModel> listDetail = query.list();
		
		for (BeliSparepartDetailModel detail : listDetail){
			SparepartModel sparepart = session.get(SparepartModel.class, detail.getSparepartId());
			// simpan data ke database
			sparepart.setStok(sparepart.getStok()-detail.getQty());
			session.update(sparepart);
			session.delete(detail);
		}
		// input data baru seperti insert
		if(model.getDetail().size() > 0){
			for (BeliSparepartDetailModel item : model.getDetail()){
				item.setBeliId(model.getId());
				// simpan data ke database
				session.save(item);
				//ambil barang
				SparepartModel sparepart = session.get(SparepartModel.class, item.getSparepartId());
				// kurangi stok
				sparepart.setStok(sparepart.getStok()+item.getQty());
				session.update(sparepart);
			}
		}
	}

	@Override
	public void delete(BeliSparepartModel model) throws Exception {
		Session session = this.sessionFactory.getCurrentSession();
		String hql= "from BeliSparepartDetailModel where beliId = :id";
		Query query = session.createQuery(hql);
		query.setInteger("id", model.getId());
		
		List<BeliSparepartDetailModel> listDetail=query.list();
		
		for (BeliSparepartDetailModel detail : listDetail){
			
			/*session.clear();*/
			Session session2 = this.sessionFactory.getCurrentSession();
			
			//ambil barang
			SparepartModel sparepart = session2.get(SparepartModel.class, detail.getSparepartId());
			// kurangi stok

			int qty = detail.getQty();
			int stok = sparepart.getStok();
			sparepart.setStok(stok - qty);
			session2.update(sparepart);
			session2.delete(detail);	
		}
		session.delete(model);

	}
	
	@Override
	public void save(BeliSparepartModel model) throws Exception {
		Session session = this.sessionFactory.getCurrentSession();
		session.save(model);
		
		List<BeliSparepartDetailModel> items = model.getDetail();
		if(items.size() > 0){
			for (BeliSparepartDetailModel item : items){
				item.setBeliId(model.getId());
				session.save(item);
				
				SparepartModel sparepart = session.get(SparepartModel.class, item.getSparepartId());
				sparepart.setStok(sparepart.getStok()+item.getQty());
				session.update(sparepart);
			}
		}

	}
	

}
