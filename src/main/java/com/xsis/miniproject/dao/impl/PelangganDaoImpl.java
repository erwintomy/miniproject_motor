package com.xsis.miniproject.dao.impl;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.xsis.miniproject.dao.PelangganDao;
import com.xsis.miniproject.model.PelangganModel;

@Repository
public class PelangganDaoImpl implements PelangganDao {
	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public List<PelangganModel> get() throws Exception {
		Session session = this.sessionFactory.getCurrentSession();
		List<PelangganModel> result = session.createQuery("select a from PelangganModel as a").list();
		return result;
	}

	@Override
	public void insert(PelangganModel model) throws Exception {
		Session session = this.sessionFactory.getCurrentSession();
		session.save(model);
	}

	@Override
	public PelangganModel getById(int id) throws Exception {
		Session session = this.sessionFactory.getCurrentSession();
		PelangganModel result = session.get(PelangganModel.class, id);
        return result;
	}

	@Override
	public void update(PelangganModel model) throws Exception {
		Session session = this.sessionFactory.getCurrentSession();
		session.update(model);
	}

	@Override
	public void delete(PelangganModel model) throws Exception {
		Session session = this.sessionFactory.getCurrentSession();
		session.delete(model);

	}
	
	@Override
	public void save(PelangganModel model) throws Exception {
		Session session = this.sessionFactory.getCurrentSession();
		session.save(model);

	}

	@Override
	public int getByKTP(String noKtp) throws Exception {
		Session session = this.sessionFactory.getCurrentSession();

		String hql = "select x from PelangganModel as x where x.noKtp = :noKtp";
		Query query = session.createQuery(hql);
		query.setParameter("noKtp", noKtp);
		
		int result = 0;
		if(query.list().size()>0){
			result = 1;
		}
		return result;
	}
	
	@Override
	public List<PelangganModel> getByTpelMon() throws Exception {
		Session session = this.sessionFactory.getCurrentSession();
		List<PelangganModel> result = session.createQuery("select a from PelangganModel as a where a.Tpel like 'Motor' ").list();
		return result;
	}
	
	@Override
	public List<PelangganModel> getByTpelSp() throws Exception {
		Session session = this.sessionFactory.getCurrentSession();
		List<PelangganModel> result = session.createQuery("select a from PelangganModel as a where a.Tpel like 'Sparepart' ").list();
		return result;
	}
}
