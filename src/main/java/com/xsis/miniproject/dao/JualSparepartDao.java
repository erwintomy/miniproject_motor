package com.xsis.miniproject.dao;

import java.util.List;

import com.xsis.miniproject.model.JualSparepartModel;

public interface JualSparepartDao {
	public List<JualSparepartModel> get() throws Exception;
	public void insert(JualSparepartModel model) throws Exception;
	public JualSparepartModel getById(int id) throws Exception;
	public int getByNo(String noStruk) throws Exception;
	public void update(JualSparepartModel model) throws Exception;
	public void delete(JualSparepartModel model) throws Exception;
	public void save(JualSparepartModel model) throws Exception;
}
