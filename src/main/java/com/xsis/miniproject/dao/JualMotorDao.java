package com.xsis.miniproject.dao;

import java.util.List;

import com.xsis.miniproject.model.JualMotorModel;

public interface JualMotorDao {
	public List<JualMotorModel> get() throws Exception;
	public void insert(JualMotorModel model) throws Exception;
	public JualMotorModel getById(int id) throws Exception;
	public void update(JualMotorModel model) throws Exception;
	public void delete(JualMotorModel model) throws Exception;
	public void save(JualMotorModel model) throws Exception;
	public int getByNoStruk(String noStruk) throws Exception;
}
