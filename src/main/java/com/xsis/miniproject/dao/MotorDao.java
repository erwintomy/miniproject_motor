package com.xsis.miniproject.dao;

import java.util.List;

import com.xsis.miniproject.model.MotorModel;

public interface MotorDao {
	public List<MotorModel> get() throws Exception;
	public void insert(MotorModel model) throws Exception;
	public MotorModel getById(int id) throws Exception;
	public int getByNama(String nama) throws Exception;
	public int getByNoRangka(String noRangka) throws Exception;
	public void update(MotorModel model) throws Exception;
	public void delete(MotorModel model) throws Exception;
	public void save(MotorModel model) throws Exception;
}
