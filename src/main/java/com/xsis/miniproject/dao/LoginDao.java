package com.xsis.miniproject.dao;

import java.util.List;
import com.xsis.miniproject.model.UsersModel;

public interface LoginDao {
	public List<UsersModel> get() throws Exception;
	public UsersModel getById(int id) throws Exception;
	public void update(UsersModel model) throws Exception;
	public void delete(UsersModel model) throws Exception;
	public void save(UsersModel model) throws Exception;
	public void insert(UsersModel model) throws Exception;
}
