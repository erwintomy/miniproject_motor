package com.xsis.miniproject.dao;

import java.util.List;

import com.xsis.miniproject.model.MerkModel;

public interface MerkDao {
	public List<MerkModel> get() throws Exception;
	public void insert(MerkModel model) throws Exception;
	public MerkModel getById(int id) throws Exception;
	public void update(MerkModel model) throws Exception;
	public void delete(MerkModel model) throws Exception;
	public void save(MerkModel model) throws Exception;
	public int getByNamaMerk(String merk) throws Exception;
}
