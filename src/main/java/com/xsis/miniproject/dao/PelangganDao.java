package com.xsis.miniproject.dao;

import java.util.List;
import com.xsis.miniproject.model.PelangganModel;

public interface PelangganDao {
	public List<PelangganModel> get() throws Exception;
	public void insert(PelangganModel model) throws Exception;
	public PelangganModel getById(int id) throws Exception;
	public int getByKTP(String noKtp) throws Exception;
	public void update(PelangganModel model) throws Exception;
	public void delete(PelangganModel model) throws Exception;
	public void save(PelangganModel model) throws Exception;
	public List<PelangganModel> getByTpelMon() throws Exception;
	public List<PelangganModel> getByTpelSp() throws Exception;
}
