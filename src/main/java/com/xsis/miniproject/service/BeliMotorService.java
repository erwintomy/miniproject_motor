package com.xsis.miniproject.service;

import java.util.List;

import com.xsis.miniproject.model.BeliMotorModel;

public interface BeliMotorService {
	public List<BeliMotorModel> get() throws Exception;
	public void insert(BeliMotorModel model) throws Exception;
	public BeliMotorModel getById(int id) throws Exception;
	public int getByNo(String noFaktur) throws Exception;
	public void update(BeliMotorModel model) throws Exception;
	public void delete(BeliMotorModel model) throws Exception;
	public void save(BeliMotorModel model) throws Exception;
}
