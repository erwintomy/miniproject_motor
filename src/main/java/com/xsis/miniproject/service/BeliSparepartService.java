package com.xsis.miniproject.service;

import java.util.List;

import com.xsis.miniproject.model.BeliSparepartModel;

public interface BeliSparepartService {
	public List<BeliSparepartModel> get() throws Exception;
	public void insert(BeliSparepartModel model) throws Exception;
	public BeliSparepartModel getById(int id) throws Exception;
	public int getByNo(String noFaktur) throws Exception;
	public void update(BeliSparepartModel model) throws Exception;
	public void delete(BeliSparepartModel model) throws Exception;
	public void save(BeliSparepartModel model) throws Exception;
}
