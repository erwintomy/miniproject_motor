package com.xsis.miniproject.service;

import java.util.List;

import com.xsis.miniproject.model.DistributorModel;
import com.xsis.miniproject.model.PelangganModel;

public interface DistributorService {
	public List<DistributorModel> get() throws Exception;
	public void insert(DistributorModel model) throws Exception;
	public DistributorModel getById(int id) throws Exception;
	public int getByName(String nama) throws Exception;
	public void update(DistributorModel model) throws Exception;
	public void delete(DistributorModel model) throws Exception;
	public void save(DistributorModel model) throws Exception;
	public List<DistributorModel> getByTdisMon() throws Exception;
	public List<DistributorModel> getByTdisSp() throws Exception;
}
