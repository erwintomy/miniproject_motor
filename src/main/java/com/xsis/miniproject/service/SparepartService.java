package com.xsis.miniproject.service;

import java.util.List;

import com.xsis.miniproject.model.SparepartModel;

public interface SparepartService {
	public List<SparepartModel> get() throws Exception;
	public void insert(SparepartModel model) throws Exception;
	public SparepartModel getById(int id) throws Exception;
	public int getByName(String nama) throws Exception;
	public void update(SparepartModel model) throws Exception;
	public void delete(SparepartModel model) throws Exception;
	public void save(SparepartModel model) throws Exception;
}
