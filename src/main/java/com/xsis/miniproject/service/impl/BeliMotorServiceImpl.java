package com.xsis.miniproject.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.xsis.miniproject.dao.BeliMotorDao;
import com.xsis.miniproject.model.BeliMotorModel;
import com.xsis.miniproject.service.BeliMotorService;

@Service
@Transactional
public class BeliMotorServiceImpl implements BeliMotorService {
    @Autowired
    private BeliMotorDao dao;
	
	@Override
	public List<BeliMotorModel> get() throws Exception {
		return this.dao.get();
	}

	@Override
	public void insert(BeliMotorModel model) throws Exception {
		this.dao.insert(model);
		
	}

	@Override
	public BeliMotorModel getById(int id) throws Exception {
		return this.dao.getById(id);
	}
	
	@Override
	public int getByNo(String noFaktur) throws Exception {
		return this.dao.getByNo(noFaktur);
	}

	@Override
	public void update(BeliMotorModel model) throws Exception {
		this.dao.update(model);
		
	}

	@Override
	public void delete(BeliMotorModel model) throws Exception {
		this.dao.delete(model);
		
	}
	
	@Override
	public void save(BeliMotorModel model) throws Exception {
		this.dao.save(model);
		
	}
}
