package com.xsis.miniproject.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.xsis.miniproject.dao.BeliSparepartDao;
import com.xsis.miniproject.model.BeliSparepartModel;
import com.xsis.miniproject.service.BeliSparepartService;

@Service
@Transactional
public class BeliSparepartServiceImpl implements BeliSparepartService {
    @Autowired
    private BeliSparepartDao dao;
	
	@Override
	public List<BeliSparepartModel> get() throws Exception {
		return this.dao.get();
	}

	@Override
	public void insert(BeliSparepartModel model) throws Exception {
		this.dao.insert(model);
		
	}

	@Override
	public BeliSparepartModel getById(int id) throws Exception {
		return this.dao.getById(id);
	}
	
	@Override
	public int getByNo(String noFaktur) throws Exception {
		return this.dao.getByNo(noFaktur);
	}

	@Override
	public void update(BeliSparepartModel model) throws Exception {
		this.dao.update(model);
		
	}

	@Override
	public void delete(BeliSparepartModel model) throws Exception {
		this.dao.delete(model);
		
	}
	
	@Override
	public void save(BeliSparepartModel model) throws Exception {
		this.dao.save(model);
		
	}
}
