package com.xsis.miniproject.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.xsis.miniproject.dao.MotorDao;
import com.xsis.miniproject.model.MotorModel;
import com.xsis.miniproject.service.MotorService;

@Service
@Transactional
public class MotorServiceImpl implements MotorService {
    @Autowired
    private MotorDao dao;
	
	@Override
	public List<MotorModel> get() throws Exception {
		return this.dao.get();
	}

	@Override
	public void insert(MotorModel model) throws Exception {
		this.dao.insert(model);
		
	}

	@Override
	public MotorModel getById(int id) throws Exception {
		return this.dao.getById(id);
	}

	@Override
	public void update(MotorModel model) throws Exception {
		this.dao.update(model);
		
	}

	@Override
	public void delete(MotorModel model) throws Exception {
		this.dao.delete(model);
		
	}
	
	@Override
	public void save(MotorModel model) throws Exception {
		this.dao.save(model);
		
	}

	@Override
	public int getByNama(String nama) throws Exception {
		return this.dao.getByNama(nama);
	}

	@Override
	public int getByNoRangka(String noRangka) throws Exception {
		return this.dao.getByNoRangka(noRangka);
	}
}
