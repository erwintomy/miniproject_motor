package com.xsis.miniproject.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.xsis.miniproject.dao.JualSparepartDao;
import com.xsis.miniproject.model.JualSparepartModel;
import com.xsis.miniproject.service.JualSparepartService;

@Service
@Transactional
public class JualSparepartServiceImpl implements JualSparepartService {
    @Autowired
    private JualSparepartDao dao;
	
	@Override
	public List<JualSparepartModel> get() throws Exception {
		return this.dao.get();
	}

	@Override
	public void insert(JualSparepartModel model) throws Exception {
		this.dao.insert(model);
		
	}

	@Override
	public JualSparepartModel getById(int id) throws Exception {
		return this.dao.getById(id);
	}
	
	@Override
	public int getByNo(String noStruk) throws Exception {
		return this.dao.getByNo(noStruk);
	}

	@Override
	public void update(JualSparepartModel model) throws Exception {
		this.dao.update(model);
		
	}

	@Override
	public void delete(JualSparepartModel model) throws Exception {
		this.dao.delete(model);
		
	}
	
	@Override
	public void save(JualSparepartModel model) throws Exception {
		this.dao.save(model);
		
	}
}
