package com.xsis.miniproject.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.xsis.miniproject.dao.JualMotorDao;
import com.xsis.miniproject.model.JualMotorModel;
import com.xsis.miniproject.service.JualMotorService;

@Service
@Transactional
public class JualMotorServiceImpl implements JualMotorService {
    @Autowired
    private JualMotorDao dao;
	
	@Override
	public List<JualMotorModel> get() throws Exception {
		return this.dao.get();
	}

	@Override
	public void insert(JualMotorModel model) throws Exception {
		this.dao.insert(model);
		
	}

	@Override
	public JualMotorModel getById(int id) throws Exception {
		return this.dao.getById(id);
	}

	@Override
	public void update(JualMotorModel model) throws Exception {
		this.dao.update(model);
		
	}

	@Override
	public void delete(JualMotorModel model) throws Exception {
		this.dao.delete(model);
		
	}
	
	@Override
	public void save(JualMotorModel model) throws Exception {
		this.dao.save(model);
		
	}

	@Override
	public int getByNoStruk(String noStruk) throws Exception {
		return this.dao.getByNoStruk(noStruk);
	}
}
