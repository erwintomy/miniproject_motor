package com.xsis.miniproject.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.xsis.miniproject.dao.SparepartDao;
import com.xsis.miniproject.model.SparepartModel;
import com.xsis.miniproject.service.SparepartService;

@Service
@Transactional
public class SparepartServiceImpl implements SparepartService {
    @Autowired
    private SparepartDao dao;
	
	
	@Override
	public List<SparepartModel> get() throws Exception {
		return this.dao.get();
	}

	@Override
	public void insert(SparepartModel model) throws Exception {
		this.dao.insert(model);
		
	}

	@Override
	public SparepartModel getById(int id) throws Exception {
		return this.dao.getById(id);
	}

	@Override
	public void update(SparepartModel model) throws Exception {
		this.dao.update(model);
		
	}

	@Override
	public void delete(SparepartModel model) throws Exception {
		this.dao.delete(model);
		
	}
	
	@Override
	public void save(SparepartModel model) throws Exception {
		this.dao.save(model);
		
	}

	@Override
	public int getByName(String nama) throws Exception {
		return this.dao.getByName(nama);
	}
}
