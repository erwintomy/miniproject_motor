package com.xsis.miniproject.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.xsis.miniproject.dao.LoginDao;
import com.xsis.miniproject.model.UsersModel;
import com.xsis.miniproject.service.LoginService;

@Service
@Transactional
public class LoginServiceImpl implements LoginService {
    @Autowired
    private LoginDao dao;
	
	
	@Override
	public List<UsersModel> get() throws Exception {
		return this.dao.get();
	}

	@Override
	public UsersModel getById(int id) throws Exception {
		return this.dao.getById(id);
	}

	@Override
	public void update(UsersModel model) throws Exception {
		this.dao.update(model);
		
	}

	@Override
	public void delete(UsersModel model) throws Exception {
		this.dao.delete(model);
		
	}
	
	@Override
	public void save(UsersModel model) throws Exception {
		this.dao.save(model);
		
	}

	@Override
	public void insert(UsersModel model) throws Exception {
		this.dao.insert(model);		
	}

}
