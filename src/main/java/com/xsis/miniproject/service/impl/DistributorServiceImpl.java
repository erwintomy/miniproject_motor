package com.xsis.miniproject.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.xsis.miniproject.dao.DistributorDao;
import com.xsis.miniproject.model.DistributorModel;
import com.xsis.miniproject.model.PelangganModel;
import com.xsis.miniproject.service.DistributorService;

@Service
@Transactional
public class DistributorServiceImpl implements DistributorService {
    @Autowired
    private DistributorDao dao;
	
	@Override
	public List<DistributorModel> get() throws Exception {
		return this.dao.get();
	}

	@Override
	public void insert(DistributorModel model) throws Exception {
		this.dao.insert(model);
		
	}
	
	@Override
	public int getByName(String nama) throws Exception {
		return this.dao.getByName(nama);
	}

	@Override
	public DistributorModel getById(int id) throws Exception {
		return this.dao.getById(id);
	}

	@Override
	public void update(DistributorModel model) throws Exception {
		this.dao.update(model);
		
	}

	@Override
	public void delete(DistributorModel model) throws Exception {
		this.dao.delete(model);
		
	}
	
	@Override
	public void save(DistributorModel model) throws Exception {
		this.dao.save(model);
		
	}
	
	@Override
	public List<DistributorModel> getByTdisMon() throws Exception {
		return this.dao.getByTdisMon();
	}
	
	@Override
	public List<DistributorModel> getByTdisSp() throws Exception {
		return this.dao.getByTdisSp();
	}
}
