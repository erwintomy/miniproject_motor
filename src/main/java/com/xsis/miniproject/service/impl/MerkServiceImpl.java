package com.xsis.miniproject.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.xsis.miniproject.dao.MerkDao;
import com.xsis.miniproject.model.MerkModel;
import com.xsis.miniproject.service.MerkService;

@Service
@Transactional
public class MerkServiceImpl implements MerkService {
    @Autowired
    private MerkDao dao;
	
	@Override
	public List<MerkModel> get() throws Exception {
		return this.dao.get();
	}

	@Override
	public void insert(MerkModel model) throws Exception {
		this.dao.insert(model);
		
	}

	@Override
	public MerkModel getById(int id) throws Exception {
		return this.dao.getById(id);
	}

	@Override
	public void update(MerkModel model) throws Exception {
		this.dao.update(model);
		
	}

	@Override
	public void delete(MerkModel model) throws Exception {
		this.dao.delete(model);
		
	}
	
	@Override
	public void save(MerkModel model) throws Exception {
		this.dao.save(model);
		
	}
	
	@Override
	public int getByNamaMerk(String merk) throws Exception {
		return this.dao.getByNamaMerk(merk);
	}
}
