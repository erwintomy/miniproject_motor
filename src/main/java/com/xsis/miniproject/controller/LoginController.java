package com.xsis.miniproject.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.xsis.miniproject.model.UsersModel;
import com.xsis.miniproject.service.LoginService;

@Controller
public class LoginController {
	private Log log = LogFactory.getLog(getClass());

	@Autowired
	private LoginService service;


	@RequestMapping(value="/login", method=RequestMethod.GET)
	public ModelAndView login(
			@RequestParam(value="error", required=false) String error,
			@RequestParam(value="logout",required=false) String logout){
		ModelAndView model = new ModelAndView();
		if(error != null){
			model.addObject("error","Invalid username and password");
		}
		if(logout != null){
			model.addObject("msg","You are have logged out successfully");
		}
		return model;
	}
	
	@RequestMapping(value="/index", method=RequestMethod.GET)
	public ModelAndView index(){
		return new ModelAndView("/index");
	}
	
	@RequestMapping(value="/select-outlet", method=RequestMethod.GET)
	public ModelAndView selectOutlet(){
		return new ModelAndView("/select-outlet");
	}
	
	@RequestMapping(value="/403", method=RequestMethod.GET)
	public ModelAndView AccessDenied(){
		ModelAndView model = new ModelAndView();
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		if(!(auth instanceof AnonymousAuthenticationToken)){
			UserDetails userDetail = (UserDetails) auth.getPrincipal();
			model.addObject("username", userDetail.getUsername());
		}
		return model;
	}
	
	/*@RequestMapping(value = "/login")
	public String index() {
		return "login";
	}*/

	@RequestMapping(value = "/login/save")
	public String save(Model model, @ModelAttribute UsersModel item, BindingResult binding, HttpServletRequest request) {
		String proses = request.getParameter("proses");
		String result ="";
		
		
		try {
			if(proses.equals("insert")){
				this.service.insert(item);
			}else if(proses.equals("update")){
				this.service.update(item);
			}else if(proses.equals("delete")){
				this.service.delete(item);
			}
			
			result="berhasil";
		} catch (Exception e) {
			log.error(e.getMessage(), e);
			result ="gagal";
		}
		model.addAttribute("result",result);
		
		return "login/save";
	}
    
	@RequestMapping(value = "/login/list")
	public String list(Model model) {
        List<UsersModel> items = null;
		
		try {
			items = this.service.get();
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}
		model.addAttribute("list", items);
		
		return "login/list";
	}
	
	@RequestMapping(value="/login/add")
	public String add(){
		return "login/add";
	}
	
	@RequestMapping(value="/login/edit")
	public String edit(Model model, HttpServletRequest request){
		// menangkap paremeter yang dikirim dari view
		int id = Integer.parseInt(request.getParameter("id"));
		
		// siapkan object fakultas model
		UsersModel item = null;
		// request ke database
		try {
			item = this.service.getById(id);
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}
		
		// datanya kita kirim ke view, 
		// kita buat variable item kemudian diisi dengan object item
		model.addAttribute("item", item);
		return "login/edit";
	}
	
	@RequestMapping(value="/login/delete")
	public String delete(Model model, HttpServletRequest request){
		// menangkap paremeter yang dikirim dari view
		int id = Integer.parseInt(request.getParameter("id"));
		
		// siapkan object fakultas model
		UsersModel item = null;
		// request ke database
		try {
			item = this.service.getById(id);
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}
		model.addAttribute("item", item);
		return "login/delete";
	}

	@RequestMapping(value = "/login/load")
	public String load(Model model) {
		List<UsersModel> data = null;
		String result = "";
		try {
			data = this.service.get();
			result = "success";

		} catch (Exception e) {
			result = "gagal";
			log.error(e.getMessage(), e);
		}

		model.addAttribute("data", data);
		model.addAttribute("result", result);

		return "login/load";
	}
	
	@RequestMapping(value = "/login/getById")
	public String getById(Model model, HttpServletRequest request) {
		UsersModel data = null;
		int id= Integer.parseInt(request.getParameter("id"));
		String result = "";
		try {
			data = this.service.getById(id);
			result = "success";

		} catch (Exception e) {
			result = "gagal";
			log.error(e.getMessage(), e);
		}
		
		model.addAttribute("data", data);
		model.addAttribute("result", result);

		return "login/getById";
	}
}
