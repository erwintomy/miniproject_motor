package com.xsis.miniproject.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.xsis.miniproject.model.MerkModel;
import com.xsis.miniproject.model.MotorModel;
import com.xsis.miniproject.model.MerkModel;
import com.xsis.miniproject.service.MerkService;

@Controller
public class MerkController {

	private Log log = LogFactory.getLog(getClass());

	@Autowired
	private MerkService service;

	@RequestMapping(value = "/merk")
	public String index() {
		return "merk";
	}

	@RequestMapping(value = "/merk/save")
	public String save(Model model, @ModelAttribute MerkModel item, BindingResult binding, HttpServletRequest request) {
		String proses = request.getParameter("proses");
		String result ="";
		
		try {
			if(proses.equals("insert")){
				this.service.insert(item);
			}else if(proses.equals("update")){
				this.service.update(item);
			}else if(proses.equals("delete")){
				this.service.delete(item);
			}
			
			result="berhasil";
		} catch (Exception e) {
			log.error(e.getMessage(), e);
			result ="gagal";
		}
		model.addAttribute("result",result);
		
		return "merk/save";
	}
    
	@RequestMapping(value = "/merk/list")
	public String list(Model model) {
		
        List<MerkModel> items = null;
		
		try {
			items = this.service.get();
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}
		model.addAttribute("list", items);
		
		return "merk/list";
	}
	
	@RequestMapping(value="/merk/add")
	public String add(){
		return "merk/add";
	}
	
	@RequestMapping(value="/merk/edit")
	public String edit(Model model, HttpServletRequest request){
		// menangkap paremeter yang dikirim dari view
		int id = Integer.parseInt(request.getParameter("id"));
		
		// siapkan object merk model
		MerkModel item = null;
		// request ke database
		try {
			item = this.service.getById(id);
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}
		
		// datanya kita kirim ke view, 
		// kita buat variable item kemudian diisi dengan object item
		model.addAttribute("item", item);
		return "merk/edit";
	}
	
	@RequestMapping(value="/merk/delete")
	public String delete(Model model, HttpServletRequest request){
		// menangkap paremeter yang dikirim dari view
		int id = Integer.parseInt(request.getParameter("id"));
		
		// siapkan object fakultas model
		MerkModel item = null;
		// request ke database
		try {
			item = this.service.getById(id);
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}
		model.addAttribute("item", item);
		return "merk/delete";
	}

	@RequestMapping(value = "/merk/load")
	public String load(Model model) {
		List<MerkModel> data = null;
		String result = "";
		try {
			data = this.service.get();
			result = "success";

		} catch (Exception e) {
			result = "gagal";
			log.error(e.getMessage(), e);
		}

		model.addAttribute("data", data);
		model.addAttribute("result", result);

		return "merk/load";
	}
	
	@RequestMapping(value = "/merk/getById")
	public String getById(Model model, HttpServletRequest request) {
		MerkModel data = null;
		int id= Integer.parseInt(request.getParameter("id"));
		String result = "";
		try {
			data = this.service.getById(id);
			result = "success";

		} catch (Exception e) {
			result = "gagal";
			log.error(e.getMessage(), e);
		}
		
		model.addAttribute("data", data);
		model.addAttribute("result", result);

		return "merk/getById";
	}
	
	@RequestMapping(value = "/merk/getByNamaMerk")
	public String getByNamaMerk(Model model, HttpServletRequest request) {
		int data = 0;
				
		String merk= request.getParameter("merk");
		String result = "";
		try {
			data = this.service.getByNamaMerk(merk);
			result = "success";

		} catch (Exception e) {
			result = "gagal";
			log.error(e.getMessage(), e);
		}
		
		model.addAttribute("data", data);
		model.addAttribute("result", result);

		return "pelanggan/getByNamaMerk";
	}
}