package com.xsis.miniproject.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.validation.constraints.Digits;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import com.xsis.miniproject.model.BeliSparepartModel;
import com.xsis.miniproject.model.DistributorModel;
import com.xsis.miniproject.model.DistributorModel;
import com.xsis.miniproject.service.DistributorService;

@Controller
public class DistributorController {

	private Log log = LogFactory.getLog(getClass());

	@Autowired
	private DistributorService service;

	@RequestMapping(value = "/distributor")
	public String index() {
		return "distributor";
	}

	@RequestMapping(value = "/distributor/save")
	public String save(Model model, @ModelAttribute DistributorModel item, BindingResult binding, HttpServletRequest request) {
		String proses = request.getParameter("proses");
		String result ="";
		try {
			if(proses.equals("insert")){
				this.service.insert(item);
			}else if(proses.equals("update")){
				this.service.update(item);
			}else if(proses.equals("delete")){
				this.service.delete(item);
			}
			result="berhasil";
		} catch (Exception e) {
			log.error(e.getMessage(), e);
			result ="gagal";
		}
		model.addAttribute("result",result);
		
		return "distributor/save";	
				
	}
	
    
	@RequestMapping(value = "/distributor/load")
	public String load(Model model) {
		List<DistributorModel> data = null;
		String result = "";
		try {
			data = this.service.get();
			result = "success";

		} catch (Exception e) {
			result = "gagal";
			log.error(e.getMessage(), e);
		}

		model.addAttribute("data", data);
		model.addAttribute("result", result);

		return "distributor/load";
	}
	
	@RequestMapping(value = "/distributor/list")
	public String list(Model model) {
        List<DistributorModel> items = null;
		
		try {
			items = this.service.get();
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}
		model.addAttribute("list", items);
		
		return "distributor/list";
	}
	
	@RequestMapping(value="/distributor/add")
	public String add(){
		return "distributor/add";
	}
	
	@RequestMapping(value="/distributor/edit")
	public String edit(Model model, HttpServletRequest request){
		// menangkap paremeter yang dikirim dari view
		int id = Integer.parseInt(request.getParameter("id"));
		
		// siapkan object fakultas model
		DistributorModel item = null;
		// request ke database
		try {
			item = this.service.getById(id);
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}
		
		// datanya kita kirim ke view, 
		// kita buat variable item kemudian diisi dengan object item
		model.addAttribute("item", item);
		return "distributor/edit";
	}
	
	@RequestMapping(value="/distributor/delete")
	public String delete(Model model, HttpServletRequest request){
		// menangkap paremeter yang dikirim dari view
		int id = Integer.parseInt(request.getParameter("id"));
		
		// siapkan object fakultas model
		DistributorModel item = null;
		// request ke database
		try {
			item = this.service.getById(id);
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}
		model.addAttribute("item", item);
		return "distributor/delete";
	}
	
	@RequestMapping(value = "/distributor/getById")
	public String getById(Model model, HttpServletRequest request) {
		DistributorModel data = null;
		int id= Integer.parseInt(request.getParameter("id"));
		String result = "";
		try {
			data = this.service.getById(id);
			result = "success";

		} catch (Exception e) {
			result = "gagal";
			log.error(e.getMessage(), e);
		}
		
		model.addAttribute("data", data);
		model.addAttribute("result", result);

		return "distributor/getById";
	}
	
	@RequestMapping(value = "/distributor/getByName")
	public String getByName(Model model, HttpServletRequest request) {
		int data = 0;
				
		String nama= request.getParameter("nama");
		String result = "";
		try {
			data = this.service.getByName(nama);
			result = "success";

		} catch (Exception e) {
			result = "gagal";
			log.error(e.getMessage(), e);
		}
		
		model.addAttribute("data", data);
		model.addAttribute("result", result);

		return "distributor/getByName";
	}
}