package com.xsis.miniproject.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import com.xsis.miniproject.model.SparepartModel;
import com.xsis.miniproject.service.SparepartService;

@Controller
public class SparepartController {

	private Log log = LogFactory.getLog(getClass());

	@Autowired
	private SparepartService service;

	@RequestMapping(value = "/sparepart")
	public String index() {
		return "sparepart";
	}

	@RequestMapping(value = "/sparepart/save")
	public String save(Model model, @ModelAttribute SparepartModel item, BindingResult binding, HttpServletRequest request) {
		String proses = request.getParameter("proses");
		String result ="";
		
		
		try {
			if(proses.equals("insert")){
				this.service.insert(item);
			}else if(proses.equals("update")){
				this.service.update(item);
			}else if(proses.equals("delete")){
				this.service.delete(item);
			}
			
			result="berhasil";
		} catch (Exception e) {
			log.error(e.getMessage(), e);
			result ="gagal";
		}
		model.addAttribute("result",result);
		
		return "sparepart/save";
	}
    
	@RequestMapping(value = "/sparepart/list")
	public String list(Model model) {
        List<SparepartModel> items = null;
		
		try {
			items = this.service.get();
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}
		model.addAttribute("list", items);
		
		return "sparepart/list";
	}
	
	@RequestMapping(value="/sparepart/add")
	public String add(){
		return "sparepart/add";
	}
	
	@RequestMapping(value="/sparepart/edit")
	public String edit(Model model, HttpServletRequest request){
		// menangkap paremeter yang dikirim dari view
		int id = Integer.parseInt(request.getParameter("id"));
		
		// siapkan object fakultas model
		SparepartModel item = null;
		// request ke database
		try {
			item = this.service.getById(id);
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}
		
		// datanya kita kirim ke view, 
		// kita buat variable item kemudian diisi dengan object item
		model.addAttribute("item", item);
		return "sparepart/edit";
	}
	
	@RequestMapping(value="/sparepart/delete")
	public String delete(Model model, HttpServletRequest request){
		// menangkap paremeter yang dikirim dari view
		int id = Integer.parseInt(request.getParameter("id"));
		
		// siapkan object fakultas model
		SparepartModel item = null;
		// request ke database
		try {
			item = this.service.getById(id);
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}
		model.addAttribute("item", item);
		return "sparepart/delete";
	}

	@RequestMapping(value = "/sparepart/load")
	public String load(Model model) {
		List<SparepartModel> data = null;
		String result = "";
		try {
			data = this.service.get();
			result = "success";

		} catch (Exception e) {
			result = "gagal";
			log.error(e.getMessage(), e);
		}

		model.addAttribute("data", data);
		model.addAttribute("result", result);

		return "sparepart/load";
	}
	
	@RequestMapping(value = "/sparepart/getById")
	public String getById(Model model, HttpServletRequest request) {
		SparepartModel data = null;
		int id= Integer.parseInt(request.getParameter("id"));
		String result = "";
		try {
			data = this.service.getById(id);
			result = "success";

		} catch (Exception e) {
			result = "gagal";
			log.error(e.getMessage(), e);
		}
		
		model.addAttribute("data", data);
		model.addAttribute("result", result);

		return "sparepart/getById";
	}
	
	@RequestMapping(value = "/sparepart/getByName")
	public String getByName(Model model, HttpServletRequest request) {
		int data = 0;
				
		String nama= request.getParameter("nama");
		String result = "";
		try {
			data = this.service.getByName(nama);
			result = "success";

		} catch (Exception e) {
			result = "gagal";
			log.error(e.getMessage(), e);
		}
		
		model.addAttribute("data", data);
		model.addAttribute("result", result);

		return "sparepart/getByName";
	}
}