package com.xsis.miniproject.controller;

import java.util.Date;
import java.text.SimpleDateFormat;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import com.xsis.miniproject.model.BeliSparepartModel;
import com.xsis.miniproject.model.DistributorModel;
import com.xsis.miniproject.model.JualSparepartModel;
import com.xsis.miniproject.model.JualSparepartModel;
import com.xsis.miniproject.model.SparepartModel;
import com.xsis.miniproject.model.PelangganModel;
import com.xsis.miniproject.service.PelangganService;
import com.xsis.miniproject.service.JualSparepartService;
import com.xsis.miniproject.service.JualSparepartService;
import com.xsis.miniproject.service.SparepartService;


@Controller
public class JualSparepartController {

	private Log log = LogFactory.getLog(getClass());

	@Autowired
	private JualSparepartService service;

	@Autowired
	private PelangganService pelService;
	
	@Autowired
	private SparepartService spService;

	@RequestMapping(value = "/jualSparepart")
	public String index() {
		return "jualSparepart";
	}

	@RequestMapping(value = "/jualSparepart/save")
	public String save(Model model, @ModelAttribute JualSparepartModel item, BindingResult binding, HttpServletRequest request) {
		String proses = request.getParameter("proses");
		String result ="";
		
		try {
			if(proses.equals("insert")){
				this.service.save(item);
			}else if(proses.equals("update")){
				this.service.update(item);
			}else if(proses.equals("delete")){
				this.service.delete(item);
			}
			
			result="berhasil";
		} catch (Exception e) {
			log.error(e.getMessage(), e);
			result ="gagal";
		}
		model.addAttribute("result",result);
		
		return "jualSparepart/save";
	}
    
	@RequestMapping(value = "/jualSparepart/list")
	public String list(Model model) {
        List<JualSparepartModel> items = null;
		
		try {
			items = this.service.get();
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}
		model.addAttribute("list", items);
		
		return "jualSparepart/list";
	}
	
	@RequestMapping(value="/jualSparepart/add")
	public String add(Model data){
		
		List<PelangganModel> listPel = null;
		List<SparepartModel> listSp = null;
		
		try {
			listPel = this.pelService.getByTpelSp();
			listSp = this.spService.get();
		} catch (Exception e) {
			
		}
		
		data.addAttribute("listPel", listPel);
		data.addAttribute("listSp", listSp);
		
		return "jualSparepart/add";
	}
	
	@RequestMapping(value="/jualSparepart/edit")
	public String edit(Model model, HttpServletRequest request){
		// menangkap paremeter yang dikirim dari view
		int id = Integer.parseInt(request.getParameter("id"));
		
		// siapkan object beli motor model
		JualSparepartModel item = null;
		List<PelangganModel> listPel = null;
		List<SparepartModel> listSp = null;
		
		try {
			listPel = this.pelService.get();
			listSp = this.spService.get();
		} catch (Exception e) {

		}
		// request ke database
		try {
			item = this.service.getById(id);
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}
		
		// datanya kita kirim ke view, 
		// kita buat variable item kemudian diisi dengan object item
		model.addAttribute("listPel", listPel);
		model.addAttribute("listSp", listSp);
		model.addAttribute("item", item);
		return "jualSparepart/edit";
	}
	
	@RequestMapping(value="/jualSparepart/delete")
	public String delete(Model model, HttpServletRequest request){
		// menangkap paremeter yang dikirim dari view
		int id = Integer.parseInt(request.getParameter("id"));
		
		// siapkan object jual model
		JualSparepartModel item = null;
		// request ke database
		try {
			item = this.service.getById(id);
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}
		model.addAttribute("item", item);
		return "jualSparepart/delete";
	}
	
	@RequestMapping(value = "/jualSparepart/load")
	public String load(Model model) {
		List<JualSparepartModel> data = null;
		String result = "";
		try {
			data = this.service.get();
			result = "success";

		} catch (Exception e) {
			result = "gagal";
			log.error(e.getMessage(), e);
		}

		model.addAttribute("data", data);
		model.addAttribute("result", result);

		return "jualSparepart/load";
	}
	
	@RequestMapping(value = "/jualSparepart/getById")
	public String getById(Model model, HttpServletRequest request) {
		JualSparepartModel data = null;
		int id= Integer.parseInt(request.getParameter("id"));
		String result = "";
		try {
			data = this.service.getById(id);
			result = "success";

		} catch (Exception e) {
			result = "gagal";
			log.error(e.getMessage(), e);
		}
		
		model.addAttribute("data", data);
		model.addAttribute("result", result);

		return "jualSparepart/getById";
	}
	
	@RequestMapping(value = "/jualSparepart/getByNo")
	public String getByNo(Model model, HttpServletRequest request) {
		int data = 0;
		
		String noStruk= request.getParameter("noStruk");
		String result = "";
		
		try {
			data = this.service.getByNo(noStruk);
			result = "berhasil";

		} catch (Exception e) {
			result = "gagal";
			log.error(e.getMessage(), e);
		}
		
		model.addAttribute("data", data);
		model.addAttribute("result", result);

		return "jualSparepart/getByNo";
	}
	
	@RequestMapping(value="/jualSparepart/detail")
	public String detail(Model model, HttpServletRequest request){
		int id = Integer.parseInt(request.getParameter("id"));

			JualSparepartModel item = null;
			List<PelangganModel> listPel = null;
			List<SparepartModel> listSp = null;
			
			String result = "";
			try {
				listPel = this.pelService.get();
				listSp = this.spService.get();
				/*item = this.service.getById(id);*/
				result = "success";

			} catch (Exception e) {
				result = "gagal";
				log.error(e.getMessage(), e);
			}
			
			try {
				item = this.service.getById(id);
			} catch (Exception e) {
				
			}
			
		model.addAttribute("listPel", listPel);
		model.addAttribute("listSp", listSp);
		model.addAttribute("item", item);
		model.addAttribute("result", result);
		return "jualSparepart/detail";
	}
	
	@InitBinder
	public void initBinder(WebDataBinder binder){
		SimpleDateFormat dateformat = new SimpleDateFormat("yyyy-MM-dd");
		dateformat.setLenient(false);
		binder.registerCustomEditor(Date.class, new CustomDateEditor(dateformat,true));
	}
}