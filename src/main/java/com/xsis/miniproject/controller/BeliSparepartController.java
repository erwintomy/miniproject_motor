package com.xsis.miniproject.controller;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import com.xsis.miniproject.model.BeliMotorModel;
import com.xsis.miniproject.model.BeliSparepartModel;
import com.xsis.miniproject.model.DistributorModel;
import com.xsis.miniproject.model.MotorModel;
import com.xsis.miniproject.model.SparepartModel;
import com.xsis.miniproject.service.BeliSparepartService;
import com.xsis.miniproject.service.DistributorService;
import com.xsis.miniproject.service.SparepartService;

@Controller
public class BeliSparepartController {

	private Log log = LogFactory.getLog(getClass());

	@Autowired
	private BeliSparepartService service;
	
	@Autowired
	private DistributorService disService;
	
	@Autowired
	private SparepartService spService;


	@RequestMapping(value = "/beliSparepart")
	public String index() {
		return "beliSparepart";
	}

	@RequestMapping(value = "/beliSparepart/save")
	public String save(Model model, @ModelAttribute BeliSparepartModel item, BindingResult binding, HttpServletRequest request) {
		String proses = request.getParameter("proses");
		String result ="";
		
		try {
			if(proses.equals("insert")){
				this.service.save(item);
			}else if(proses.equals("update")){
				this.service.update(item);
			}else if(proses.equals("delete")){
				this.service.delete(item);
			}
			
			result="berhasil";
		} catch (Exception e) {
			log.error(e.getMessage(), e);
			result ="gagal";
		}
		model.addAttribute("result",result);
		
		return "beliSparepart/save";
	}
    
	@RequestMapping(value = "/beliSparepart/list")
	public String list(Model model) {
        List<BeliSparepartModel> items = null;
		
		try {
			items = this.service.get();
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}
		model.addAttribute("list", items);
		
		return "beliSparepart/list";
	}
	
	@RequestMapping(value="/beliSparepart/add")
	public String add(Model data){
		
		List<DistributorModel> listDis = null;
		List<SparepartModel> listSp = null;
		
		try {
			listDis = this.disService.getByTdisSp();
			listSp = this.spService.get();
		} catch (Exception e) {
			
		}
		
		data.addAttribute("listDis", listDis);
		data.addAttribute("listSp", listSp);
		
		return "beliSparepart/add";
	}
	
	@RequestMapping(value="/beliSparepart/edit")
	public String edit(Model model, HttpServletRequest request){
		// menangkap paremeter yang dikirim dari view
		int id = Integer.parseInt(request.getParameter("id"));
		
		BeliSparepartModel item = null;
		List<DistributorModel> listDis = null;
		List<SparepartModel> listSp = null;
		
		try {
			listDis = this.disService.get();
			listSp = this.spService.get();
		} catch (Exception e) {

		}
		
		try {
			item = this.service.getById(id);
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}
		
		// datanya kita kirim ke view, 
		// kita buat variable item kemudian diisi dengan object item
		model.addAttribute("listDis", listDis);
		model.addAttribute("listSp", listSp);
		model.addAttribute("item", item);
		return "beliSparepart/edit";
	}
	
	@RequestMapping(value="/beliSparepart/delete")
	public String delete(Model model, HttpServletRequest request){
		// menangkap paremeter yang dikirim dari view
		int id = Integer.parseInt(request.getParameter("id"));
		
		// siapkan object fakultas model
		BeliSparepartModel item = null;
		// request ke database
		try {
			item = this.service.getById(id);
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}
		model.addAttribute("item", item);
		return "beliSparepart/delete";
	}
	
	@RequestMapping(value = "/beliSparepart/load")
	public String load(Model model) {
		List<BeliSparepartModel> data = null;
		String result = "";
		try {
			data = this.service.get();
			result = "success";

		} catch (Exception e) {
			result = "gagal";
			log.error(e.getMessage(), e);
		}

		model.addAttribute("data", data);
		model.addAttribute("result", result);

		return "beliSparepart/load";
	}
	
	@RequestMapping(value = "/beliSparepart/getById")
	public String getById(Model model, HttpServletRequest request) {
		BeliSparepartModel data = null;
		int id= Integer.parseInt(request.getParameter("id"));
		String result = "";
		try {
			data = this.service.getById(id);
			result = "success";

		} catch (Exception e) {
			result = "gagal";
			log.error(e.getMessage(), e);
		}
		
		model.addAttribute("data", data);
		model.addAttribute("result", result);

		return "beliSparepart/getById";
	}
	
	@RequestMapping(value = "/beliSparepart/getByNo")
	public String getByNo(Model model, HttpServletRequest request) {
		int data = 0;
				
		String noFaktur= request.getParameter("noFaktur");
		String result = "";
		try {
			data = this.service.getByNo(noFaktur);
			result = "success";

		} catch (Exception e) {
			result = "gagal";
			log.error(e.getMessage(), e);
		}
		
		model.addAttribute("data", data);
		model.addAttribute("result", result);

		return "beliSparepart/getByNo";
	}
	
	@RequestMapping(value="/beliSparepart/detail")
	public String detail(Model model, HttpServletRequest request){
		int id = Integer.parseInt(request.getParameter("id"));

			BeliSparepartModel item = null;
			List<DistributorModel> listDis = null;
			List<SparepartModel> listSp = null;
			
			String result = "";
			try {
				listDis = this.disService.get();
				listSp = this.spService.get();
				/*item = this.service.getById(id);*/
				result = "success";

			} catch (Exception e) {
				result = "gagal";
				log.error(e.getMessage(), e);
			}
			
			try {
				item = this.service.getById(id);
			} catch (Exception e) {
				
			}
			
		model.addAttribute("listDis", listDis);
		model.addAttribute("listSp", listSp);
		model.addAttribute("item", item);
		model.addAttribute("result", result);
		return "beliSparepart/detail";
	}
	
	@InitBinder
	public void initBinder(WebDataBinder binder){
		SimpleDateFormat dateformat = new SimpleDateFormat("yyyy-MM-dd");
		dateformat.setLenient(false);
		binder.registerCustomEditor(Date.class, new CustomDateEditor(dateformat,true));
	}
}