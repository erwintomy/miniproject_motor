package com.xsis.miniproject.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import com.xsis.miniproject.model.MerkModel;
import com.xsis.miniproject.model.MotorModel;
import com.xsis.miniproject.service.MerkService;
import com.xsis.miniproject.service.MotorService;

@Controller
public class MotorController {

	private Log log = LogFactory.getLog(getClass());

	@Autowired
	private MotorService service;
    
	@Autowired
	private MerkService merkService;
	
	@RequestMapping(value = "/motor")
	public String index() {
		return "motor";
	}

	@RequestMapping(value = "/motor/save")
	public String save(Model model, @ModelAttribute MotorModel item, BindingResult binding, HttpServletRequest request) {
		String proses = request.getParameter("proses");
		String result ="";
		
		try {
			if(proses.equals("insert")){
				this.service.insert(item);
			}else if(proses.equals("update")){
				this.service.update(item);
			}else if(proses.equals("delete")){
				this.service.delete(item);
			}
			
			result="berhasil";
		} catch (Exception e) {
			log.error(e.getMessage(), e);
			result ="gagal";
		}
		model.addAttribute("result",result);
		
		return "motor/save";
	}
    
	@RequestMapping(value = "/motor/list")
	public String list(Model model) {
        List<MotorModel> items = null;
		
		try {
			items = this.service.get();
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}
		model.addAttribute("list", items);
		
		return "motor/list";
	}
	
	@RequestMapping(value="/motor/add")
    public String add(Model data){
		
		List<MerkModel> listMerk = null;
		
		try {
			listMerk = this.merkService.get();
		} catch (Exception e) {
			
		}
		
		data.addAttribute("listMerk", listMerk);
		
		return "motor/add";
	}
	
	@RequestMapping(value="/motor/edit")
	public String edit(Model model, HttpServletRequest request){
		// menangkap paremeter yang dikirim dari view
		int id = Integer.parseInt(request.getParameter("id"));
	
		MotorModel item = null;
		List<MerkModel> listMerk = null;
		try {
			listMerk = this.merkService.get();
		} catch (Exception e) {
			
		}
		try {
			item = this.service.getById(id);
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}
		
		// datanya kita kirim ke view, 
		// kita buat variable item kemudian diisi dengan object item
		model.addAttribute("listMerk", listMerk);
		model.addAttribute("item", item);
		return "motor/edit";
	}
	
	@RequestMapping(value="/motor/delete")
	public String delete(Model model, HttpServletRequest request){
		// menangkap paremeter yang dikirim dari view
		int id = Integer.parseInt(request.getParameter("id"));
		
		// siapkan object fakultas model
		MotorModel item = null;
		// request ke database
		try {
			item = this.service.getById(id);
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}
		model.addAttribute("item", item);
		return "motor/delete";
	}

	@RequestMapping(value = "/motor/load")
	public String load(Model model) {
		List<MotorModel> data = null;
		String result = "";
		try {
			data = this.service.get();
			result = "success";

		} catch (Exception e) {
			result = "gagal";
			log.error(e.getMessage(), e);
		}

		model.addAttribute("data", data);
		model.addAttribute("result", result);

		return "motor/load";
	}
	
	@RequestMapping(value = "/motor/getById")
	public String getById(Model model, HttpServletRequest request) {
		MotorModel data = null;
		int id= Integer.parseInt(request.getParameter("id"));
		String result = "";
		try {
			data = this.service.getById(id);
			result = "success";

		} catch (Exception e) {
			result = "gagal";
			log.error(e.getMessage(), e);
		}
		
		model.addAttribute("data", data);
		model.addAttribute("result", result);

		return "motor/getById";
	}
	
	@RequestMapping(value = "/motor/getByNama")
	public String getByNama(Model model, HttpServletRequest request) {
		int data = 0;
		
		String nama= request.getParameter("nama");
		String result = "";
		
		try {
			data = this.service.getByNama(nama);
			result = "berhasil";

		} catch (Exception e) {
			result = "gagal";
			log.error(e.getMessage(), e);
		}
		
		model.addAttribute("data", data);
		model.addAttribute("result", result);

		return "motor/getByNama";
	}
	
	@RequestMapping(value = "/motor/getByNoRangka")
	public String getByNoRangka(Model model, HttpServletRequest request) {
		int data = 0;
		
		String noRangka= request.getParameter("noRangka");
		String result = "";
		
		try {
			data = this.service.getByNoRangka(noRangka);
			result = "berhasil";

		} catch (Exception e) {
			result = "gagal";
			log.error(e.getMessage(), e);
		}
		
		model.addAttribute("data", data);
		model.addAttribute("result", result);

		return "motor/getByNoRangka";
	}
}