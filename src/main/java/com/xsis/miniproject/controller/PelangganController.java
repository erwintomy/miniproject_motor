package com.xsis.miniproject.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import com.xsis.miniproject.model.PelangganModel;
import com.xsis.miniproject.model.PelangganModel;
import com.xsis.miniproject.service.PelangganService;

@Controller
public class PelangganController {

	private Log log = LogFactory.getLog(getClass());

	@Autowired
	private PelangganService service;

	@RequestMapping(value = "/pelanggan")
	public String index() {
		return "pelanggan";
	}

	@RequestMapping(value = "/pelanggan/save")
	public String save(Model model, @ModelAttribute PelangganModel item, BindingResult binding, HttpServletRequest request) {
		String proses = request.getParameter("proses");
		String result ="";
		
		try {
			if(proses.equals("insert")){
				this.service.insert(item);
			}else if(proses.equals("update")){
				this.service.update(item);
			}else if(proses.equals("delete")){
				this.service.delete(item);
			}
			
			result="berhasil";
		} catch (Exception e) {
			log.error(e.getMessage(), e);
			result ="gagal";
		}
		model.addAttribute("result",result);
		
		return "pelanggan/save";
	}
    
	@RequestMapping(value = "/pelanggan/list")
	public String list(Model model) {
        List<PelangganModel> items = null;
		
		try {
			items = this.service.get();
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}
		model.addAttribute("list", items);
		
		return "pelanggan/list";
	}
	
	@RequestMapping(value="/pelanggan/add")
	public String add(){
		return "pelanggan/add";
	}
	
	@RequestMapping(value="/pelanggan/edit")
	public String edit(Model model, HttpServletRequest request){
		// menangkap paremeter yang dikirim dari view
		int id = Integer.parseInt(request.getParameter("id"));
		
		// siapkan object fakultas model
		PelangganModel item = null;
		// request ke database
		try {
			item = this.service.getById(id);
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}
		
		// datanya kita kirim ke view, 
		// kita buat variable item kemudian diisi dengan object item
		model.addAttribute("item", item);
		return "pelanggan/edit";
	}
	
	@RequestMapping(value="/pelanggan/delete")
	public String delete(Model model, HttpServletRequest request){
		// menangkap paremeter yang dikirim dari view
		int id = Integer.parseInt(request.getParameter("id"));
		
		// siapkan object fakultas model
		PelangganModel item = null;
		// request ke database
		try {
			item = this.service.getById(id);
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}
		model.addAttribute("item", item);
		return "pelanggan/delete";
	}
	@RequestMapping(value = "/pelanggan/load")
	public String load(Model model) {
		List<PelangganModel> data = null;
		String result = "";
		try {
			data = this.service.get();
			result = "success";

		} catch (Exception e) {
			result = "gagal";
			log.error(e.getMessage(), e);
		}

		model.addAttribute("data", data);
		model.addAttribute("result", result);

		return "pelanggan/load";
	}
	
	@RequestMapping(value = "/pelanggan/getById")
	public String getById(Model model, HttpServletRequest request) {
		PelangganModel data = null;
		int id= Integer.parseInt(request.getParameter("id"));
		String result = "";
		try {
			data = this.service.getById(id);
			result = "success";

		} catch (Exception e) {
			result = "gagal";
			log.error(e.getMessage(), e);
		}
		
		model.addAttribute("data", data);
		model.addAttribute("result", result);

		return "pelanggan/getById";
	}
	
	@RequestMapping(value = "/pelanggan/getByKTP")
	public String getByKTP(Model model, HttpServletRequest request) {
		int data = 0;
				
		String noKtp= request.getParameter("noKtp");
		String result = "";
		try {
			data = this.service.getByKTP(noKtp);
			result = "success";

		} catch (Exception e) {
			result = "gagal";
			log.error(e.getMessage(), e);
		}
		
		model.addAttribute("data", data);
		model.addAttribute("result", result);

		return "pelanggan/getByKTP";
	}
}