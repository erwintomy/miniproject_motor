package com.xsis.miniproject.controller;

import java.util.Date;
import java.text.SimpleDateFormat;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import com.xsis.miniproject.model.BeliMotorModel;
import com.xsis.miniproject.model.DistributorModel;
import com.xsis.miniproject.model.JualMotorModel;
import com.xsis.miniproject.model.MotorModel;
import com.xsis.miniproject.model.PelangganModel;
import com.xsis.miniproject.service.JualMotorService;
import com.xsis.miniproject.service.MotorService;
import com.xsis.miniproject.service.PelangganService;

@Controller
public class JualMotorController {

	private Log log = LogFactory.getLog(getClass());

	@Autowired
	private JualMotorService service;

	@Autowired
	private PelangganService pelService;
	
	@Autowired
	private MotorService monService;

	@RequestMapping(value = "/jualMotor")
	public String index() {
		return "jualMotor";
	}

	@RequestMapping(value = "/jualMotor/save")
	public String save(Model model, @ModelAttribute JualMotorModel item, BindingResult binding, HttpServletRequest request) {
		String proses = request.getParameter("proses");
		String result ="";
		
		try {
			if(proses.equals("insert")){
				this.service.save(item);
			}else if(proses.equals("update")){
				this.service.update(item);
			}else if(proses.equals("delete")){
				this.service.delete(item);
			}
			
			result="berhasil";
		} catch (Exception e) {
			log.error(e.getMessage(), e);
			result ="gagal";
		}
		model.addAttribute("result",result);
		
		return "jualMotor/save";
	}
    
	@RequestMapping(value = "/jualMotor/list")
	public String list(Model model) {
        List<JualMotorModel> items = null;
		
		try {
			items = this.service.get();
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}
		model.addAttribute("list", items);
		
		return "jualMotor/list";
	}
	
	@RequestMapping(value="/jualMotor/add")
	public String add(Model data){
		
		List<PelangganModel> listPel = null;
		List<MotorModel> listMon = null;
		
		try {
			listPel = this.pelService.getByTpelMon();
			listMon = this.monService.get();
		} catch (Exception e) {
			
		}
		
		data.addAttribute("listPel", listPel);
		data.addAttribute("listMon", listMon);
		
		return "jualMotor/add";
	}
	
	@RequestMapping(value="/jualMotor/edit")
	public String edit(Model model, HttpServletRequest request){
		int id = Integer.parseInt(request.getParameter("id"));
		
		JualMotorModel item = null;
		List<PelangganModel> listPel = null;
		List<MotorModel> listMon = null;
		
		try {
			listPel = this.pelService.getByTpelMon();
			listMon = this.monService.get();
		} catch (Exception e) {

		}
		// request ke database
		try {
			item = this.service.getById(id);
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}
		
		// datanya kita kirim ke view, 
		// kita buat variable item kemudian diisi dengan object item
		model.addAttribute("listPel", listPel);
		model.addAttribute("listMon", listMon);
		model.addAttribute("item", item);
		return "jualMotor/edit";
	}
	
	@RequestMapping(value="/jualMotor/delete")
	public String delete(Model model, HttpServletRequest request){
		// menangkap paremeter yang dikirim dari view
		int id = Integer.parseInt(request.getParameter("id"));
		
		// siapkan object jual model
		JualMotorModel item = null;
		// request ke database
		try {
			item = this.service.getById(id);
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}
		model.addAttribute("item", item);
		return "jualMotor/delete";
	}
	
	@RequestMapping(value = "/jualMotor/load")
	public String load(Model model) {
		List<JualMotorModel> data = null;
		String result = "";
		try {
			data = this.service.get();
			result = "success";

		} catch (Exception e) {
			result = "gagal";
			log.error(e.getMessage(), e);
		}

		model.addAttribute("data", data);
		model.addAttribute("result", result);

		return "jualMotor/load";
	}
	
	@RequestMapping(value = "/jualMotor/getById")
	public String getById(Model model, HttpServletRequest request) {
		JualMotorModel data = null;
		int id= Integer.parseInt(request.getParameter("id"));
		String result = "";
		try {
			data = this.service.getById(id);
			result = "success";

		} catch (Exception e) {
			result = "gagal";
			log.error(e.getMessage(), e);
		}
		
		model.addAttribute("data", data);
		model.addAttribute("result", result);

		return "jualMotor/getById";
	}
	
	@RequestMapping(value = "/jualMotor/getByNoStruk")
	public String getByNoStruk(Model model, HttpServletRequest request) {
		int data = 0;
		
		String noStruk= request.getParameter("noStruk");
		String result = "";
		
		try {
			data = this.service.getByNoStruk(noStruk);
			result = "berhasil";

		} catch (Exception e) {
			result = "gagal";
			log.error(e.getMessage(), e);
		}
		
		model.addAttribute("data", data);
		model.addAttribute("result", result);

		return "jualMotor/getByNoStruk";
	}
	
	@RequestMapping(value="/jualMotor/detail")
	public String detail(Model model, HttpServletRequest request){
		int id = Integer.parseInt(request.getParameter("id"));

			JualMotorModel item = null;
			List<PelangganModel> listPel = null;
			List<MotorModel> listMon = null;
			
			String result = "";
			try {
				listPel = this.pelService.get();
				listMon = this.monService.get();
				/*item = this.service.getById(id);*/
				result = "success";

			} catch (Exception e) {
				result = "gagal";
				log.error(e.getMessage(), e);
			}
			
			try {
				item = this.service.getById(id);
			} catch (Exception e) {
				
			}
			
		model.addAttribute("listPel", listPel);
		model.addAttribute("listMon", listMon);
		model.addAttribute("item", item);
		model.addAttribute("result", result);
		return "jualMotor/detail";
	}
	
	@InitBinder
	public void initBinder(WebDataBinder binder){
		SimpleDateFormat dateformat = new SimpleDateFormat("yyyy-MM-dd");
		dateformat.setLenient(false);
		binder.registerCustomEditor(Date.class, new CustomDateEditor(dateformat,true));
	}
}